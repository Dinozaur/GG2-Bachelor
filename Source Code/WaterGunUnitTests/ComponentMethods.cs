﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun;
using WaterGun.Entities.Components;
using WaterGun.Maths;

namespace WaterGunUnitTests
{
    [TestClass]
    public class ComponentMethods
    {
        private GraphicsComponent graphicsComponent;
        private int imageWidth, imageHeight;
        private Color topLeft, topRight, bottomLeft, bottomRight;

        [TestInitialize]
        public void Init()
        {
            Bitmap testImage = new Bitmap(Directory.GetCurrentDirectory() +  "/TestAssets/4colors.png");
            Animation anim = new Animation(testImage);
            graphicsComponent = new GraphicsComponent("test",anim);
            testImage = (Bitmap)graphicsComponent.GetImage(); //<-- this is done, as graphicsComponent preps its blueprint for rotation

            imageWidth = testImage.Width;
            imageHeight = testImage.Height;

            //since graphicsComponent leaves some transparent pixels as border; use the center quater of the image to get corner colors
            topLeft =       testImage.GetPixel(1 * imageWidth / 4, 1 * imageHeight / 4);
            topRight =      testImage.GetPixel(3 * imageWidth / 4, 1 * imageHeight / 4);
            bottomLeft =    testImage.GetPixel(1 * imageWidth / 4, 3 * imageHeight / 4);
            bottomRight =   testImage.GetPixel(3 * imageWidth / 4, 3 * imageHeight / 4);
        }

        [TestMethod]
        public void GraphicsComponentRotateImage()
        {
            int assertCount = 0;
            //0 degrees:
            Vector2d deg0 = new Vector2d(1, 0); //<--- 0 degrees = pointing right
            graphicsComponent.RotateImage(deg0, false);

            using (Bitmap test1 = new Bitmap(graphicsComponent.GetImage()))
            {
                //see Init() for an explanation of the coordinates
                Color tl = test1.GetPixel(1 * imageWidth / 4, 1 * imageHeight / 4);
                Color tr = test1.GetPixel(3 * imageWidth / 4, 1 * imageHeight / 4);
                Color bl = test1.GetPixel(1 * imageWidth / 4, 3 * imageHeight / 4);
                Color br = test1.GetPixel(3 * imageWidth / 4, 3 * imageHeight / 4);

                Assert.AreEqual(tl, topLeft, "assert failed: " + assertCount++);
                Assert.AreEqual(tr, topRight, "assert failed: " + assertCount++);
                Assert.AreEqual(bl, bottomLeft, "assert failed: " + assertCount++);
                Assert.AreEqual(br, bottomRight, "assert failed: " + assertCount++);
            }

            //90 degrees:
            Vector2d deg90 = new Vector2d(0, 1);
            graphicsComponent.RotateImage(deg90, false);

            using (Bitmap test2 = new Bitmap(graphicsComponent.GetImage()))
            {
                //see Init() for an explanation of the coordinates
                Color tl = test2.GetPixel(1 * imageWidth / 4, 1 * imageHeight / 4);
                Color tr = test2.GetPixel(3 * imageWidth / 4, 1 * imageHeight / 4);
                Color bl = test2.GetPixel(1 * imageWidth / 4, 3 * imageHeight / 4);
                Color br = test2.GetPixel(3 * imageWidth / 4, 3 * imageHeight / 4);

                Assert.AreEqual(tr, topLeft, "assert failed: " + assertCount++);
                Assert.AreEqual(br, topRight, "assert failed: " + assertCount++);
                Assert.AreEqual(tl, bottomLeft, "assert failed: " + assertCount++);
                Assert.AreEqual(bl, bottomRight, "assert failed: " + assertCount++);
            }

            //45 degrees
            Vector2d deg45 = new Vector2d(1, 1);
            graphicsComponent.RotateImage(deg45, false);

            using (Bitmap test3 = new Bitmap(graphicsComponent.GetImage()))
            {
                //See Init() for an explanation of coordinates
                Color t = test3.GetPixel(   imageWidth / 2          ,   1 * imageHeight / 4 );
                Color r = test3.GetPixel(   3 * imageWidth / 4      ,   imageHeight / 2     );
                Color b = test3.GetPixel(   imageWidth / 2          ,   3 * imageHeight / 4 );
                Color l = test3.GetPixel(   1 * imageWidth / 4      ,   imageHeight / 2     );

                Assert.AreEqual(t, topLeft, "assert failed: " + assertCount++);
                Assert.AreEqual(r, topRight, "assert failed: " + assertCount++);
                Assert.AreEqual(l, bottomLeft, "assert failed: " + assertCount++);
                Assert.AreEqual(b, bottomRight, "assert failed: " + assertCount++);
            }
        }
    }
}
