﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WaterGun.Entities;
using WaterGunUnitTests.TestClasses;

namespace WaterGunUnitTests
{
    [TestClass]
    public class ComponentPattern
    {
        private Entity ent;

        [TestInitialize]
        public void Init()
        {
            ent = new TestEntity("test" + 0);
            TestComponent tcomp = new TestComponent();
            ent.AddComponent(tcomp);
        }

        [TestMethod]
        public void HasComponent()
        {
            Assert.IsTrue(ent.HasComponent<TestComponent>());
            Assert.IsFalse(ent.HasComponent<TestComponent2>());   
        }

        [TestMethod]
        public void HasInheritedComponent()
        {
            Assert.IsTrue(ent.HasComponent<TestComponentInterface>());
            Assert.IsFalse(ent.HasComponent<TestComponent2>());
        }

        [TestMethod]
        public void GetComponent()
        {
            Assert.IsNotNull(ent.GetComponent<TestComponent>());
            Assert.IsNull(ent.GetComponent<TestComponent2>());
        }

        [TestMethod]
        public void GetInheritedComponent()
        {
            Assert.IsNotNull(ent.GetComponent<TestComponentInterface>());
            Assert.IsNull(ent.GetComponent<TestComponent2>());
        }

        [TestMethod]
        public void AddComponent()
        {
            Entity ent = new TestEntity("test" + 0);
            Assert.IsFalse(ent.HasComponent<TestComponent>());
            ent.AddComponent(new TestComponent());
            Assert.IsTrue(ent.HasComponent<TestComponent>());
        }
    }
}
