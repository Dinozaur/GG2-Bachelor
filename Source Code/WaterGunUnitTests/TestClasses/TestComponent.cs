﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities;
using WaterGun.Entities.Components;

namespace WaterGunUnitTests.TestClasses
{
    public class TestComponent : IGameComponent, TestComponentInterface
    {
        //inherited from TestComponentInterface
        public void SomeMethod()
        {
            throw new NotImplementedException(); //this method is not meant to be called
        }
    }
}
