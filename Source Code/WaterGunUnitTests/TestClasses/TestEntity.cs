﻿using GameSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities;

namespace WaterGunUnitTests.TestClasses
{
    class TestEntity : Entity
    {
        public TestEntity(string name) : base(name, false)
        {

        }

        public override string GetPrefix()
        {
            return "Test";
        }

        public override WGObject Serialize()
        {
            throw new NotImplementedException();
        }

        public override void Update(WGObject wgoEntities)
        {
            throw new NotImplementedException();
        }
    }
}
