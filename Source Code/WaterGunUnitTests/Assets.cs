﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Sprites;

namespace WaterGunUnitTests
{
    [TestClass]
    public class Assets
    {
        [TestMethod]
        public void SpriteSheetLoad()
        {
            bool result = false;

            try
            {
                SpriteSheet ss = new SpriteSheet("/TestAssets/SpriteSheet.png");
                ss.Load();
                result = true;
            }
            catch
            {
                
            }

            Assert.IsTrue(result, "Loading failed for some reason.");
        }

        [TestMethod]
        public void GetSprite()
        {
            Image img1 = null;
            Image img2 = null;

            try
            {
                SpriteSheet ss = new SpriteSheet("/TestAssets/SpriteSheet.png");
                ss.Load();

                img1 = ss.GetSprite(0, 3, 16);
                img2 = ss.GetSprite(1, 3, 16);

            }
            catch
            {

            }

            Assert.IsTrue(img1 != null && img2 != null, "One of the images are null");
            Assert.AreNotEqual(img1, img2, "The images are the same");
        }
    }
}
