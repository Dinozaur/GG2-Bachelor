﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WaterForge.Controller;
using WaterForge.Model;

namespace WaterForge.GUI
{
    public partial class WaterForgeUI : Form
    {
        private StoryController controller;

        public WaterForgeUI()
        {
            InitializeComponent();
            controller = new StoryController();
            controller.Load();

            FillListStory(controller.GetAllStories());
            
        }

        private void FillListStory(Story[] stories)
        {
            lstStory.Items.Clear();

            foreach (Story story in stories)
            {
                ListViewItem item = new ListViewItem();
                item.SubItems.Add(story.ID.ToString());
                item.SubItems.Add(story.Name);
                this.lstStory.Items.Add(item);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            StoryUI sui = new StoryUI(controller);
            sui.Show();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            controller.Save();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if(lstStory.SelectedItems.Count > 0)
            {
                int id = Int32.Parse(lstStory.SelectedItems[0].SubItems[1].Text);

                Story story = controller.GetStory(id);

                StoryUI sui = new StoryUI(story, controller);
                sui.Show();
            }
        }

        private void Refresh()
        {
            lstStory.Items.Clear();
            FillListStory(controller.GetAllStories());
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Refresh();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lstStory.SelectedItems.Count > 0)
            {
                int id = Int32.Parse(lstStory.SelectedItems[0].SubItems[1].Text);
                controller.RemoveStory(id);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtBoxSearch.Text.Equals(""))
            {
                Refresh();
            }
            else
            {
                int id = Int32.Parse(txtBoxSearch.Text);
                Story[] story = new Story[] { controller.GetStory(id) };

                FillListStory(story);
            }
        }
    }
}
