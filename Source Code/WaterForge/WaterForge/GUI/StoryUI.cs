﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WaterForge.Model;
using WaterForge.Controller;

namespace WaterForge.GUI
{
    public partial class StoryUI : Form
    {
        private StoryController controller;

        private Story data;

        public StoryUI(StoryController controller)
        {
            InitializeComponent();

            txtID.Text = "N/A";

            this.controller = controller;
        }

        public StoryUI(Story story, StoryController controller)
        {
            InitializeComponent();

            txtID.Text = story.ID + "";
            txtName.Text = story.Name;
            txtPriority.Text = story.Priority;
            txtDescription.Text = story.Description;
            txtEstimate.Text = story.EstimatedHours + "";

            this.controller = controller;
            data = story;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (data == null)
            {
                controller.AddStory(txtName.Text, txtPriority.Text, Double.Parse(txtEstimate.Text), txtDescription.Text);
            }
            else
            {
                data.Description = txtDescription.Text;
                data.EstimatedHours = Double.Parse(txtEstimate.Text); //TODO: check?
                data.Name = txtName.Text;
                data.Priority = txtPriority.Text;
                controller.Edit(data);
            }

            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
