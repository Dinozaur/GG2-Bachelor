﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WaterForge.Model
{
    [Serializable]
    public class Story
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public int ID { get; set; }
        [XmlAttribute]
        public string Priority { get; set; }
        [XmlAttribute]
        public double EstimatedHours { get; set; }
        [XmlAttribute]
        public string Description { get; set; }

        public Story()
        {

        }

        public Story(string name, int id, string priority, double estimatedhours, string description)
        {
            Name = name;
            ID = id;
            Priority = priority;
            EstimatedHours = estimatedhours;
            Description = description;
        }
    }
}
