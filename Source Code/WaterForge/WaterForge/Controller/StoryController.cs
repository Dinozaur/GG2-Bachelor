﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using WaterForge.Model;

namespace WaterForge.Controller
{
    public class StoryController
    {
        private Dictionary<int, Story> backlog;
        private int nextID;

        public StoryController()
        {
            backlog = new Dictionary<int, Story>();
            Load();
        }

        private void LoadNextID()
        {
            int maxID = 0;

            foreach(int id in backlog.Keys)
            {
                maxID = id > maxID ? id : maxID;
            }

            nextID = maxID;
        }

        public void AddStory(string name, string priority, double estimatedhours, string description)
        {
            nextID++;

            Story story = new Story(name, nextID, priority, estimatedhours, description);

            backlog.Add(story.ID, story);
        }

        public void RemoveStory(int id)
        {
            backlog.Remove(id);
        }

        public void Save()
        {
            XmlSerializer xmlS = new XmlSerializer(typeof(XMLDictionaryItem[]), new XmlRootAttribute() { ElementName = "stories" });

            string cPath = Directory.GetCurrentDirectory();

            using (FileStream fs = new FileStream(cPath + "/Data/PBlog.xml", FileMode.Create))
            {
                xmlS.Serialize(fs, backlog.Select((entry) => new XMLDictionaryItem() { id = entry.Key, story = entry.Value }).ToArray());
            }
        }

        /// <summary>
        /// Changes the story (found by ID) in memory! It does not save to file!
        /// </summary>
        /// <param name="story">The changed story to save in memory</param>
        public void Edit(Story story)
        {
            backlog[story.ID] = story;
        }

        public void Load()
        {
            XmlSerializer xmlS = new XmlSerializer(typeof(XMLDictionaryItem[]), new XmlRootAttribute() { ElementName = "stories" });

            
            string cPath = Directory.GetCurrentDirectory();
            string dPath = cPath + "/Data/";
            string fPath = dPath +"PBlog.xml";
            

            if(!Directory.Exists(dPath))
            {
                Directory.CreateDirectory(dPath);
            }

            if(File.Exists(fPath))
            {
                using (FileStream fs = new FileStream(fPath, FileMode.OpenOrCreate))
                {
                    XMLDictionaryItem[] items = (XMLDictionaryItem[])xmlS.Deserialize(fs);
                    backlog = items.ToDictionary((i) => i.id, (i) => i.story);
                }
            }

            LoadNextID();

        }

        public Story[] GetAllStories()
        {
            return backlog.Values.ToArray();
        }

        public Story GetStory(int id)
        {
            return backlog[id];
        }
    }
}
