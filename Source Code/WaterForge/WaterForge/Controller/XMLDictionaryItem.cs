﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using WaterForge.Model;

namespace WaterForge.Controller
{
    [Serializable]
    public class XMLDictionaryItem
    {
        [XmlAttribute]
        public int id;
        [XmlElement]
        public Story story;
    }
}
