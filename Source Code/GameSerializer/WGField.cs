﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSerializer
{
    public class WGField
    {
       
        private WGField()
        {

        }

        public byte Type { get; private set; }
        public short NameLength { get; private set; }
        public char[] Name { get; private set; }
        public byte[] Data { get; private set; }

        public WGField(string name, byte value)
        {
            Type = (byte)PrimitiveTypes.BYTE;
            NameLength = (short)name.Length;
            Name = name.ToCharArray();
            Data = new byte[SerializerUtil.TypeSize(Type)];
            SerializerUtil.FillBytes(Data, 0, value);
        }

        public WGField(string name, bool value)
        {
            Type = (byte)PrimitiveTypes.BOOLEAN;
            NameLength = (short)name.Length;
            Name = name.ToCharArray();
            Data = new byte[SerializerUtil.TypeSize(Type)];
            SerializerUtil.FillBytes(Data, 0, value);
        }

        public WGField(string name, short value)
        {
            Type = (byte)PrimitiveTypes.SHORT;
            NameLength = (short)name.Length;
            Name = name.ToCharArray();
            Data = new byte[SerializerUtil.TypeSize(Type)];
            SerializerUtil.FillBytes(Data, 0, value);
        }

        public WGField(string name, char value)
        {
            Type = (byte)PrimitiveTypes.CHAR;
            NameLength = (short)name.Length;
            Name = name.ToCharArray();
            Data = new byte[SerializerUtil.TypeSize(Type)];
            SerializerUtil.FillBytes(Data, 0, value);
        }

        public WGField(string name, int value)
        {
            Type = (byte)PrimitiveTypes.INT;
            NameLength = (short)name.Length;
            Name = name.ToCharArray();
            Data = new byte[SerializerUtil.TypeSize(Type)];
            SerializerUtil.FillBytes(Data, 0, value);
        }

        public WGField(string name, long value)
        {
            Type = (byte)PrimitiveTypes.LONG;
            NameLength = (short)name.Length;
            Name = name.ToCharArray();
            Data = new byte[SerializerUtil.TypeSize(Type)];
            SerializerUtil.FillBytes(Data, 0, value);
        }

        public WGField(string name, float value)
        {
            Type = (byte)PrimitiveTypes.FLOAT;
            NameLength = (short)name.Length;
            Name = name.ToCharArray();
            Data = new byte[SerializerUtil.TypeSize(Type)];
            SerializerUtil.FillBytes(Data, 0, value);
        }

        public WGField(string name, double value)
        {
            Type = (byte)PrimitiveTypes.DOUBLE;
            NameLength = (short)name.Length;
            Name = name.ToCharArray();
            Data = new byte[SerializerUtil.TypeSize(Type)];
            SerializerUtil.FillBytes(Data, 0, value);
        }

        public int GetBytes(byte[] dest, int pointer)
        {

            pointer = SerializerUtil.FillBytes(dest, pointer, Type);
            pointer = SerializerUtil.FillBytes(dest, pointer, NameLength);
            pointer = SerializerUtil.FillBytes(dest, pointer, Name);
            pointer = SerializerUtil.FillBytes(dest, pointer, Data);

            return pointer;
        }

        public static WGField Deserialize(byte[] data, ref int pointer)
        {
            byte type = SerializerUtil.ReadByte(data, ref pointer);
            short nameLength = SerializerUtil.ReadShort(data, ref pointer);

            char[] name = new char[nameLength];
            for (short i = 0; i < nameLength; i++)
            {
                name[i] = SerializerUtil.ReadChar(data, ref pointer);
            }

            WGField result = null;

            switch (type)
            {
                case (byte)PrimitiveTypes.BOOLEAN:
                    bool boolValue = SerializerUtil.ReadBool(data, ref pointer);
                    result = new WGField(new string(name), boolValue);
                    break;
                case (byte)PrimitiveTypes.BYTE:
                    byte byteValue = SerializerUtil.ReadByte(data, ref pointer);
                    result = new WGField(new string(name), byteValue);
                    break;
                case (byte)PrimitiveTypes.CHAR:
                    char charValue = SerializerUtil.ReadChar(data, ref pointer);
                    result = new WGField(new string(name), charValue);
                    break;
                case (byte)PrimitiveTypes.DOUBLE:
                    double doubleValue = SerializerUtil.ReadDouble(data, ref pointer);
                    result = new WGField(new string(name), doubleValue);
                    break;
                case (byte)PrimitiveTypes.FLOAT:
                    float floatValue = SerializerUtil.ReadFloat(data, ref pointer);
                    result = new WGField(new string(name), floatValue);
                    break;
                case (byte)PrimitiveTypes.INT:
                    int intValue = SerializerUtil.ReadInt(data, ref pointer);
                    result = new WGField(new string(name), intValue);
                    break;
                case (byte)PrimitiveTypes.LONG:
                    long longValue = SerializerUtil.ReadLong(data, ref pointer);
                    result = new WGField(new string(name), longValue);
                    break;
                case (byte)PrimitiveTypes.SHORT:
                    short shortValue = SerializerUtil.ReadShort(data, ref pointer);
                    result = new WGField(new string(name), shortValue);
                    break;
                case (byte)PrimitiveTypes.UNKNOWN:
                    throw new GameSerializerException("GameSerilizer tried to serialize an unknown primitive type to a field at " + pointer, data, pointer);
                    break;
                default:
                    throw new GameSerializerException("GameSerilizer something went wrong while serilizing a field at: " + pointer, data, pointer);
                    break;

            }

            return result;
        }

        public int GetSize()
        {
            return 1 +                                                          // type
                2 +                                                             // name length
                NameLength * SerializerUtil.TypeSize(PrimitiveTypes.CHAR) +     // name
                SerializerUtil.TypeSize(Type);                                  // data
        }

        public override string ToString()
        {

            return "WGF: " + new string(Name);
        }
    }
}

