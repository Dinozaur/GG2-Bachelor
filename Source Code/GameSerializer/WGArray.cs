﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSerializer
{
    public class WGArray
    {
        private WGArray(char[] name, byte itemType, short count, byte[] data)
        {
            Init(name);

            ItemType = itemType;
            Count = count;
            Data = data;
        }

        public WGArray(string name, byte[] data)
        {
            Init(name);

            ItemType = (byte)PrimitiveTypes.BYTE;
            Count = (short)data.Length;
            Data = data;
        }

        public WGArray(string name, bool[] data)
        {
            Init(name);

            ItemType = (byte)PrimitiveTypes.BOOLEAN;
            Count = (short)data.Length;

            byte[] bdata = new byte[Count];
            SerializerUtil.FillBytes(bdata, 0, data);

            Data = bdata;
        }

        public WGArray(string name, short[] data)
        {
            Init(name);
            ItemType = (byte)PrimitiveTypes.SHORT;
            Count = (short)data.Length;

            byte[] bdata = new byte[SerializerUtil.TypeSize(ItemType) * Count];
            SerializerUtil.FillBytes(bdata, 0, data);

            Data = bdata;
        }

        public WGArray(string name, char[] data)
        {
            Init(name);
            ItemType = (byte)PrimitiveTypes.CHAR;
            Count = (short)data.Length;

            byte[] bdata = new byte[SerializerUtil.TypeSize(ItemType) * Count];
            SerializerUtil.FillBytes(bdata, 0, data);

            Data = bdata;
        }

        public WGArray(string name, int[] data)
        {
            Init(name);
            ItemType = (byte)PrimitiveTypes.INT;
            Count = (short)data.Length;

            byte[] bdata = new byte[SerializerUtil.TypeSize(ItemType) * Count];
            SerializerUtil.FillBytes(bdata, 0, data);

            Data = bdata;
        }

        public WGArray(string name, long[] data)
        {
            Init(name);
            ItemType = (byte)PrimitiveTypes.LONG;
            Count = (short)data.Length;

            byte[] bdata = new byte[SerializerUtil.TypeSize(ItemType) * Count];
            SerializerUtil.FillBytes(bdata, 0, data);

            Data = bdata;
        }

        public WGArray(string name, float[] data)
        {
            Init(name);
            ItemType = (byte)PrimitiveTypes.FLOAT;
            Count = (short)data.Length;

            byte[] bdata = new byte[SerializerUtil.TypeSize(ItemType) * Count];
            SerializerUtil.FillBytes(bdata, 0, data);

            Data = bdata;
        }

        public WGArray(string name, double[] data)
        {
            Init(name);
            ItemType = (byte)PrimitiveTypes.DOUBLE;
            Count = (short)data.Length;

            byte[] bdata = new byte[SerializerUtil.TypeSize(ItemType) * Count];
            SerializerUtil.FillBytes(bdata, 0, data);

            Data = bdata;
        }
        
        public byte Type { get; private set; }
        public short NameLength { get; private set; }
        public char[] Name { get; private set; }
        public byte ItemType { get; private set; }
        public short Count { get; private set; }
        public byte[] Data { get; private set; }

        public int GetBytes(byte[] dest, int pointer)
        {
            pointer = SerializerUtil.FillBytes(dest, pointer, Type);
            pointer = SerializerUtil.FillBytes(dest, pointer, NameLength);
            pointer = SerializerUtil.FillBytes(dest, pointer, Name);
            pointer = SerializerUtil.FillBytes(dest, pointer, ItemType);
            pointer = SerializerUtil.FillBytes(dest, pointer, Count);
            pointer = SerializerUtil.FillBytes(dest, pointer, Data);

            return pointer;
        }


        public static WGArray Deserialize(byte[] data, ref int pointer)
        {
            byte type = SerializerUtil.ReadByte(data, ref pointer);

            if (type != (byte)ContainerTypes.ARRAY)
            {
                string msg = String.Format("Array Type mismatch! \r\n Byte[] Type: {0} \r\n Serializer Type: {1}", type, (byte)ContainerTypes.ARRAY);
                throw new GameSerializerException(msg, data, pointer);
            }

            short nameLength = SerializerUtil.ReadShort(data, ref pointer);

            char[] name = new char[nameLength];
            for (short i = 0; i < nameLength; i++)
            {
                name[i] = SerializerUtil.ReadChar(data, ref pointer);
            }

            byte itemType = SerializerUtil.ReadByte(data, ref pointer);

            short elementCount = SerializerUtil.ReadShort(data, ref pointer);

            int bytelength = elementCount * SerializerUtil.TypeSize(itemType);

            byte[] arrData = new byte[bytelength];

            for(int i = 0; i < bytelength; i++)
            {
                arrData[i] = SerializerUtil.ReadByte(data, ref pointer);
            }

            return new WGArray(name, itemType, elementCount, arrData);
        }

        private void Init(char[] name)
        {
            Type = (byte)ContainerTypes.ARRAY;
            NameLength = (short)name.Length;
            Name = name;
        }

        private void Init(string name)
        {
            Init(name.ToCharArray());
        }

        public int GetSize()
        {
            return 1 +              //type
                2 +                 //name length
                NameLength  * 2 +   //characters * char size
                1 +                 //item type
                2 +                 //element count
                Count * SerializerUtil.TypeSize(ItemType);
        }

        public override string ToString()
        {
            return "WGA: " + new string(Name);
        }
    }
}
