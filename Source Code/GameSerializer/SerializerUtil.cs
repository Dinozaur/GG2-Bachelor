﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSerializer
{
    public enum PrimitiveTypes
    {
        UNKNOWN = 0,
        BYTE = 1,
        BOOLEAN = 2,
        CHAR = 3,
        SHORT = 4,
        INT = 5,
        LONG = 6,
        FLOAT = 7,
        DOUBLE = 8,
    }

    public enum ContainerTypes
    {
        UNKNOWN = 0,
        OBJECT = 1,
        ARRAY = 2,
        DATABASE = 3
    }

    public class SerializerUtil
    {
        public static byte TypeSize(byte type)
        {
            return TypeSize((PrimitiveTypes)type);
        }

        public static byte TypeSize(PrimitiveTypes type)
        {
            byte res = 0;
            switch (type)
            {
                case PrimitiveTypes.BYTE:
                case PrimitiveTypes.BOOLEAN:
                    res = (byte)1;
                    break;
                case PrimitiveTypes.SHORT:
                case PrimitiveTypes.CHAR:
                    res = (byte)2;
                    break;
                case PrimitiveTypes.INT:
                case PrimitiveTypes.FLOAT:
                    res = (byte)4;
                    break;
                case PrimitiveTypes.LONG:
                case PrimitiveTypes.DOUBLE:
                    res = (byte)8;
                    break;
            }

            return res;
        }

        public static int FillBytes(byte[] dest, int pointer, byte src)
        {
            dest[pointer++] = src;
            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, bool src)
        {
            dest[pointer++] = src ? (byte)1 : (byte)0;

            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, short src)
        {
            dest[pointer++] = (byte)((src >> 0));
            dest[pointer++] = (byte)((src >> 8));

            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, char src)
        {
            dest[pointer++] = (byte)((src >> 0));
            dest[pointer++] = (byte)((src >> 8));

            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, int src)
        {
            dest[pointer++] = (byte)((src >> 0));
            dest[pointer++] = (byte)((src >> 8));
            dest[pointer++] = (byte)((src >> 16));
            dest[pointer++] = (byte)((src >> 24));

            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, long src)
        {
            dest[pointer++] = (byte)((src >> 0 * 8));
            dest[pointer++] = (byte)((src >> 1 * 8));
            dest[pointer++] = (byte)((src >> 2 * 8));
            dest[pointer++] = (byte)((src >> 3 * 8));
            dest[pointer++] = (byte)((src >> 4 * 8));
            dest[pointer++] = (byte)((src >> 5 * 8));
            dest[pointer++] = (byte)((src >> 6 * 8));
            dest[pointer++] = (byte)((src >> 7 * 8));

            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, float src)
        {
            byte[] srcb = System.BitConverter.GetBytes(src);
            dest[pointer++] = srcb[0];
            dest[pointer++] = srcb[1];
            dest[pointer++] = srcb[2];
            dest[pointer++] = srcb[3];

            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, double src)
        {
            byte[] srcb = System.BitConverter.GetBytes(src);
            dest[pointer++] = srcb[0];
            dest[pointer++] = srcb[1];
            dest[pointer++] = srcb[2];
            dest[pointer++] = srcb[3];
            dest[pointer++] = srcb[4];
            dest[pointer++] = srcb[5];
            dest[pointer++] = srcb[6];
            dest[pointer++] = srcb[7];

            return pointer;
        }



        public static int FillBytes(byte[] dest, int pointer, byte[] src)
        {
            for (int i = 0; i < src.Length; i++)
            {
                dest[pointer++] = src[i];
            }

            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, bool[] src)
        {
            for (int i = 0; i < src.Length; i++)
            {
                pointer = FillBytes(dest, pointer, src[i]);
            }

            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, short[] src)
        {
            for (int i = 0; i < src.Length; i++)
            {
                pointer = FillBytes(dest, pointer, src[i]);
            }

            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, char[] src)
        {
            for (int i = 0; i < src.Length; i++)
            {
                pointer = FillBytes(dest, pointer, src[i]);
            }
            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, int[] src)
        {
            for (int i = 0; i < src.Length; i++)
            {
                pointer = FillBytes(dest, pointer, src[i]);
            }
            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, long[] src)
        {
            for (int i = 0; i < src.Length; i++)
            {
                pointer = FillBytes(dest, pointer, src[i]);
            }
            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, float[] src)
        {
            for (int i = 0; i < src.Length; i++)
            {
                pointer = FillBytes(dest, pointer, src[i]);
            }
            return pointer;
        }

        public static int FillBytes(byte[] dest, int pointer, double[] src)
        {
            for (int i = 0; i < src.Length; i++)
            {
                pointer = FillBytes(dest, pointer, src[i]);
            }
            return pointer;
        }

        public static byte ReadByte(byte[] src, ref int pointer)
        {
            pointer++;
            return src[pointer - 1];
        }

        public static bool ReadBool(byte[] src, ref int pointer)
        {
            pointer++;
            return src[pointer - 1] > 0;
        }

        public static short ReadShort(byte[] src, ref int pointer)
        {
            return (short)(
                src[pointer++]    << 0 | 
                src[pointer++]  << 8);
        }

        public static char ReadChar(byte[] src, ref int pointer)
        {
            return (char)(
                src[pointer++]    << 0 |
                src[pointer++]  << 8);
        }

        //TODO: test this! (post-incrementation in or-sequence
        public static int ReadInt(byte[] src, ref int pointer)
        {
            return (int)(
                src[pointer++] << 0 |
                src[pointer++] << 8 |
                src[pointer++] << 16 |
                src[pointer++] << 24);
        }

        public static long ReadLong(byte[] src, ref int pointer)
        {
            return (long)(
                src[pointer++] << (0 * 8) |
                src[pointer++] << (1 * 8) |
                src[pointer++] << (2 * 8) |
                src[pointer++] << (3 * 8) |
                src[pointer++] << (4 * 8) |
                src[pointer++] << (5 * 8) |
                src[pointer++] << (6 * 8) |
                src[pointer++] << (7 * 8));
        }

        public static float ReadFloat(byte[] src, ref int pointer)
        {
            byte[] s = new byte[4]{src[pointer++], src[pointer++], src[pointer++], src[pointer++]};
            float res = BitConverter.ToSingle(s, 0); //<-- TODO: replace 0 with pointer?
            return res;
        }

        public static double ReadDouble(byte[] src, ref int pointer)
        {
            byte[] s = new byte[8] { src[pointer++], src[pointer++], src[pointer++], src[pointer++], src[pointer++], src[pointer++], src[pointer++], src[pointer++] };
            double res = BitConverter.ToDouble(s, 0); //<-- TODO: replace 0 with pointer?
            return res;
        }

        public static string ReadString(byte[] scr, ref int pointer, int characterCount)
        {
            string res = "";

            for (int i = 0; characterCount > i; i++)
            {
                char c = ReadChar(scr, ref pointer);
                res += c;
            }

            return res;
        }

        public static byte[] CleanBytes(byte[] data)
        {
            byte[] headerBytes = new byte[WGDatabase.HEADER.Length * TypeSize(PrimitiveTypes.CHAR)];
            FillBytes(headerBytes, 0, WGDatabase.HEADER);

            int headerIndex = LocateIndexOfMatch(data, headerBytes, 0);

            if (headerIndex != -1)
            {
                byte[] footerBytes = new byte[WGDatabase.FOOTER.Length * TypeSize(PrimitiveTypes.CHAR)];
                FillBytes(footerBytes, 0, WGDatabase.FOOTER);

                int footerIndex = LocateIndexOfMatch(data, footerBytes, headerIndex);

                if (footerIndex != -1)
                {
                    byte[] result = new byte[(footerIndex + TypeSize(PrimitiveTypes.CHAR) * WGDatabase.FOOTER.Length) - headerIndex];
                    for (int i = 0; i < result.Length; i++)
                    {
                        result[i] = data[headerIndex + i];
                    }
                    return result;
                }
            }
            return null;
        }

        private static int LocateIndexOfMatch(byte[] haystack, byte[] needle, int pointerOffset)
        {
            bool found = false;
            int i = pointerOffset;

            while(!found && i < haystack.Length)
            { 
                byte straw = haystack[i];
                int needleIndex = 0;
                while(needleIndex < needle.Length && straw == needle[needleIndex] && haystack.Length > i + needleIndex)
                {
                    needleIndex++;
                    if(needleIndex == needle.Length)
                    {
                        found = true;
                    }
                    else if(haystack.Length > i + needleIndex)
                    {
                        straw = haystack[i + needleIndex];
                    }
                }
                if (!found)
                {
                    i++;
                }
            }

            if(found)
            {
                return i;
            }
            else
            {
                return -1;
            }
        }
    }
}
