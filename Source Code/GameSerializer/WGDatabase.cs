﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSerializer
{
    public class WGDatabase
    {
        public static readonly char[] HEADER = new char[] { 'W', 'G' };
        public static readonly short VERSION = 0x0002;
        public static readonly char[] FOOTER = new char[] { 'G', 'W' };

        private WGDatabase(short nameLength, char[] name)
        {
            Objects = new List<WGObject>();

            Type = (byte)ContainerTypes.DATABASE;
            NameLength = nameLength;
            Name = name;

            Size = CalcStartingSize(name);
        }

        public WGDatabase(string name)
        {
            Objects = new List<WGObject>();

            Type = (byte)ContainerTypes.DATABASE;
            Size = CalcStartingSize(name.ToCharArray());
            NameLength = (short)name.Length;
            Name = name.ToCharArray();
        }

        public byte Type { get; private set; }
        public short NameLength { get; private set; }
        public char[] Name { get; private set; }
        public int Size { get; private set; } //<-- in bytes
        public List<WGObject> Objects { get; private set; }

        public void Add(WGObject obj)
        {
            Objects.Add(obj);
            Size += obj.Size;
        }

        public void RecalculateSize()
        {
            Size = CalcStartingSize(Name);

            foreach (WGObject o in Objects)
            {
                o.RecalculateSize();
                Size += o.Size;
            }
        }

        public byte[] SerializeToBytes()
        {
            RecalculateSize();
            byte[] res = new byte[Size];

            int pointer = 0; //<-- i.e. index in result

            pointer = SerializerUtil.FillBytes(res, 0, HEADER);
            pointer = SerializerUtil.FillBytes(res, pointer, VERSION);
            pointer = SerializerUtil.FillBytes(res, pointer, NameLength);
            pointer = SerializerUtil.FillBytes(res, pointer, Name);
            pointer = SerializerUtil.FillBytes(res, pointer, Size);
            pointer = SerializerUtil.FillBytes(res, pointer, Objects.Count);
            foreach (WGObject o in Objects)
            {
                pointer = o.GetBytes(res, pointer);
            }
            SerializerUtil.FillBytes(res, pointer, FOOTER);
            return res;
        }

        public void SerializeToFile(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    bw.Write(SerializeToBytes());
                }
            }
        }

        public WGObject FindObject(string name)
        {
            bool found = false;
            short index = 0;
            while (!found && index < Objects.Count)
            {
                WGObject o = Objects[index];
                found = new String(o.Name).Equals(name);
                if (!found)
                {
                    index++;
                }
            }

            if (found)
            {
                return Objects[index];
            }
            else
            {
                return null;
            }
        }

        public static WGDatabase Deserialize(byte[] data)
        {
            int pointer = 0;
            char[] header = new char[HEADER.Length];

            for (byte i = 0; i < HEADER.Length; i++)
            {
                header[i] = SerializerUtil.ReadChar(data, ref pointer);
            }

            if (!(new string(header).Equals(new string(HEADER))))
            {
                string msg = String.Format("Header mismatch! \r\n Byte[] Header: {0} \r\n Serializer Header: {1}", new string(header), new string(HEADER));
                throw new GameSerializerException(msg, data, pointer);
            }

            short version = SerializerUtil.ReadShort(data, ref pointer);

            if (version != VERSION)
            {
                string msg = String.Format("Version mismatch! \r\n Byte[] Version: {0} \r\n Serializer Version: {1}", version, VERSION);
                throw new GameSerializerException(msg, data, pointer);
            }

            short nameSize = SerializerUtil.ReadShort(data, ref pointer);
            char[] name = new char[nameSize];

            for (short i = 0; i < name.Length; i++)
            {
                name[i] = SerializerUtil.ReadChar(data, ref pointer);
            }

            int size = SerializerUtil.ReadInt(data, ref pointer);

            WGDatabase result = new WGDatabase(nameSize, name);

            int objectCount = SerializerUtil.ReadInt(data, ref pointer);

            for (int i = 0; i < objectCount; i++)
            {
                result.Add(WGObject.Deserialize(data, ref pointer));
            }

            char[] footer = new char[FOOTER.Length];

            for (byte i = 0; i < FOOTER.Length; i++)
            {
                footer[i] = SerializerUtil.ReadChar(data, ref pointer);
            }

            if (!(new string(footer).Equals(new string(FOOTER))))
            {
                string msg = String.Format("Footer mismatch! \r\n Byte[] Footer: {0} \r\n Serializer Footer: {1}", new string(footer), new string(FOOTER));
                throw new GameSerializerException(msg, data, pointer);
            }

            if (size != result.Size)
            {
                string msg = String.Format("Size mismatch! \r\n Byte[] Size: {0} \r\n Serializer Size: {1}", size, result.Size);
                throw new GameSerializerException(msg, data, pointer);
            }

            return result;
        }

        public static WGDatabase Deserialize(string path)
        {
            byte[] data = null;
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Open))
                {
                    data = new byte[fs.Length];
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        //TODO: read a full long if nessesary (two ints)
                        data = br.ReadBytes((int)fs.Length);
                    }
                }

            }
            //TODO: find other possible exceptions and make a proper error for them
            catch (FileNotFoundException fnfe)
            {
                WGDatabase errordb = new WGDatabase("Serialization Error");
                WGObject wgo = new WGObject("Description");
                wgo.AddArray(new WGArray("Error", "FileNotFoundException".ToCharArray()));
                wgo.AddArray(new WGArray("Path", path.ToCharArray()));
                errordb.Add(wgo);

                return errordb;
            }

            return Deserialize(data);
        }

        private int CalcStartingSize(char[] name)
        {
            return HEADER.Length * SerializerUtil.TypeSize(PrimitiveTypes.CHAR) +  // header
                   SerializerUtil.TypeSize(PrimitiveTypes.SHORT) +                 // version
                   FOOTER.Length * SerializerUtil.TypeSize(PrimitiveTypes.CHAR) +  // footer
                   2 +                                                             // name length
                   name.Length * SerializerUtil.TypeSize(PrimitiveTypes.CHAR) +    // name
                   4 +                                                             // size
                   4;                                                              // object count


        }

        public override string ToString()
        {
            return new string(Name);
        }
    }
}
