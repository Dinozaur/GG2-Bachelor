﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSerializer
{
    public class GameSerializerException : Exception
    {
        public GameSerializerException(string message, byte[] data, int pointer)
            : base(message)
        {
            Data = data;
        }

        public byte[] Data { get; private set; }
        public int Pointer { get; private set; }
    }
}
