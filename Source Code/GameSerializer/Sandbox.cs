﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSerializer
{
    public class Sandbox
    {
        public static void Main(string[] args)
        {
            /*
            WGDatabase db = new WGDatabase("Update");
            WGObject player = new WGObject("Player");
            
            //player.AddField(WGField.Generic<short>("ID", 33));
            
            WGObject position = new WGObject("Position");
            position.AddField(new WGField("X", (double)460));
            position.AddField(new WGField("Y", 460.5));
            
            player.AddObject(position);
             
            player.AddArray(new WGArray("b", new bool[] { false, true}));
            db.Add(player);

            db.SerializeToFile(Directory.GetCurrentDirectory() + "/data/test.wgd");

            WGDatabase dsdb = WGDatabase.Deserialize(Directory.GetCurrentDirectory() + "/data/test.wgd");
            WGObject dsPlayer =  dsdb.FindObject("Player");
            WGObject dsPosition = dsPlayer.FindObject("Position");
            
            Console.WriteLine("Hello World");
             */


            WGDatabase db = new WGDatabase("t");
            WGObject player = new WGObject("Player");
            db.Add(player);
            WGObject location = new WGObject("Location");
            player.AddObject(location);

            location.AddField(new WGField("X", (double)400));
            location.AddField(new WGField("Y", (double)600));

            byte[] data = db.SerializeToBytes();


            Console.WriteLine("test");
        }
    }
}
