﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameSerializer
{
    public class WGObject
    {
        private WGObject(short nameLength, char[] name)
        {
            Fields = new List<WGField>();
            Arrays = new List<WGArray>();
            Objects = new List<WGObject>();

            Type = (byte)ContainerTypes.OBJECT;
            NameLength = nameLength;
            Name = name;
            Size = CalculateStartingSize();
        }

        public WGObject(string name)
        {
            Type = (byte)ContainerTypes.OBJECT;
            NameLength = (short)name.Length;
            Name = name.ToCharArray();
            Fields = new List<WGField>();
            Arrays = new List<WGArray>();
            Objects = new List<WGObject>();

            Size = CalculateStartingSize();
        }

        public void ChangeName(string newName)
        {
            NameLength = (short)newName.Length;
            Name = newName.ToCharArray();
            RecalculateSize();
        }

        private int CalculateStartingSize()
        {
            return 1 +              //type
                4 +                 // Size
                2 +                 //name length
                NameLength * 2 +    //name * char size
                2 +                 //fields count
                2 +                 //arrays count
                2;                  //objects count

        }

        public byte Type { get; private set; }
        public short NameLength { get; private set; }
        public char[] Name { get; private set; }
        public List<WGField> Fields { get; private set; }
        public List<WGArray> Arrays {  get; private set; }
        public List<WGObject> Objects { get; private set; }
        public int Size { get; private set; }

        public WGObject FindObject(string name)
        {
            bool found = false;
            short index = 0;
            while (!found && index < Objects.Count)
            {
                WGObject o = Objects[index];
                found = new String(o.Name).Equals(name);
                if (!found)
                {
                    index++;
                }
            }

            if (found)
            {
                return Objects[index];
            }
            else
            {
                return null;
            }
        }

        public void AddField(WGField field)
        {
            Fields.Add(field);
            Size += field.GetSize();
        }

        public void AddArray(WGArray array)
        {
            Arrays.Add(array);
            Size += array.GetSize();
        }

        public void AddObject(WGObject obj)
        {
            Objects.Add(obj);
            Size += obj.Size;
        }

        public int GetBytes(byte[] dest, int pointer)
        {            
            pointer = SerializerUtil.FillBytes(dest, pointer, Type);
            pointer = SerializerUtil.FillBytes(dest, pointer, Size);
            pointer = SerializerUtil.FillBytes(dest, pointer, NameLength);
            pointer = SerializerUtil.FillBytes(dest, pointer, Name);
            pointer = SerializerUtil.FillBytes(dest, pointer, (short)Fields.Count);

            foreach (WGField field in Fields)
            {
                pointer = field.GetBytes(dest, pointer);
            }


            pointer = SerializerUtil.FillBytes(dest, pointer, (short)Arrays.Count);

            foreach (WGArray array in Arrays)
            {
                pointer = array.GetBytes(dest, pointer);
            }

            pointer = SerializerUtil.FillBytes(dest, pointer, (short)Objects.Count);

            foreach (WGObject obj in Objects)
            {
                pointer = obj.GetBytes(dest, pointer);
            }

            return pointer;
        }


        public static WGObject Deserialize(byte[] data, ref int pointer)
        {
            byte type = SerializerUtil.ReadByte(data, ref pointer);
            if (type != (byte)ContainerTypes.OBJECT)
            {
                string msg = String.Format("Object Type mismatch! \r\n Byte[] Type: {0} \r\n Serializer Type: {1}", type, (byte)ContainerTypes.OBJECT);
                throw new GameSerializerException(msg, data, pointer);
            }

            int size = SerializerUtil.ReadInt(data, ref pointer);

            short nameLength = SerializerUtil.ReadShort(data, ref pointer);
            
            char[] name = new char[nameLength];
            for (short i = 0; i < nameLength; i++)
            {
                name[i] = SerializerUtil.ReadChar(data, ref pointer);
            }

            WGObject result = new WGObject(nameLength, name);

            short fieldCount = SerializerUtil.ReadShort(data, ref pointer);

            for (short i = 0; i < fieldCount; i++)
            {
                result.AddField(WGField.Deserialize(data, ref pointer));
            }

            short arrayCount = SerializerUtil.ReadShort(data, ref pointer);

            for (short i = 0; i < arrayCount; i++)
            {
                result.AddArray(WGArray.Deserialize(data, ref pointer));
            }

            short objCount = SerializerUtil.ReadShort(data, ref pointer);

            for (short i = 0; i < objCount; i++)
            {
                result.AddObject(WGObject.Deserialize(data, ref pointer));
            }

            return result;

        }

        public WGArray FindArray(string name)
        {
            bool found = false;
            short index = 0;
            while (!found && index < Arrays.Count)
            {
                WGArray a = Arrays[index];
                found = new String(a.Name).Equals(name);
                if (!found)
                {
                    index++;
                }
            }

            if (found)
            {
                return Arrays[index];
            }
            else
            {
                return null;
            }
        }

        public WGField FindField(string name)
        {
            bool found = false;
            short index = 0;
            while (!found && index < Fields.Count)
            {
                WGField f = Fields[index];
                found = new String(f.Name).Equals(name);
                if (!found)
                {
                    index++;
                }
            }

            if (found)
            {
                return Fields[index];
            }
            else
            {
                return null;
            }
        
        }

        public void RecalculateSize()
        {
            Size = CalculateStartingSize();

            foreach (WGField field in Fields)
            {
                Size += field.GetSize();
            }

            foreach (WGArray array in Arrays)
            {
                Size += array.GetSize();
            }

            foreach (WGObject obj in Objects)
            {
                obj.RecalculateSize();
                Size += obj.Size;
            }
        }

        public override string ToString()
        {
            string name = new string(Name);
            return "WGO: " + name;
        }
    }
}
