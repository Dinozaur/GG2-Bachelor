﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun
{
    public class GameTimer
    {
        public GameTimer()
        {
            DeltaTime = -1;
            TimeFactor = -1;
        }

        public int DeltaTime { get; private set; }
        public double TimeFactor { get; private set; }

        public void Set(int deltaTime, double timeFactor)
        {
            DeltaTime = deltaTime;
            TimeFactor = timeFactor;
        }
    }
}
