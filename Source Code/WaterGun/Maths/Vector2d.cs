﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Maths
{
    public class Vector2d
    {
        //caching stuff
        private double radians;
        private bool radianCalculated;

        public Vector2d(double x, double y)
        {
            radianCalculated = false;
            X = x;
            Y = y;
        }

        public double X { get; private set; }
        public double Y { get; private set; }

        public static Vector2d operator +(Vector2d v1, Vector2d v2)
        {
            return new Vector2d(v1.X + v2.X, v1.Y + v2.Y);
        }

        public static Vector2d operator -(Vector2d v1, Vector2d v2)
        {
            return new Vector2d(v1.X - v2.X, v1.Y - v2.Y);
        }

        public static Vector2d operator *(Vector2d v, double c)
        {
            return new Vector2d(v.X * c, v.Y * c);
        }

        public static explicit operator Point(Vector2d v)
        {
            return new Point((int)v.X, (int)v.Y);
        }

        public double CalcMagnitude()
        {
            return Math.Sqrt(X * X + Y * Y);
        }

        public double InRadians()
        {
            if(!radianCalculated)
            {
                radians = Math.Atan2(Y, X);
                radianCalculated = true;
            }
            return radians;
        }

        public double InDegrees()
        {
            double rad = InRadians();
            double deg = rad * 180 / Math.PI;
            return deg;
        }

        public Vector2d Copy()
        {
            return new Vector2d(X, Y);
        }

        public override string ToString()
        {
            return String.Format("({0};{1})", X, Y);
        }
    }
}
