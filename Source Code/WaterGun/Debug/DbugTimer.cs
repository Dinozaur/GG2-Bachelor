﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Debug
{
    /// <summary>
    /// Contains static functionality to time multiple things with only one timer
    /// </summary>
    public class DbugTimer
    {
        private static Stopwatch timer; //the ongoing timer. This stopwatch will never be stopped while the program runs.

        private static Dictionary<string, TimeLog> timelogs;

        /// <summary>
        /// This will only happen once.
        /// </summary>
        static DbugTimer()
        {
            timer = new Stopwatch();
            timer.Start();
            timelogs = new Dictionary<string, TimeLog>();
        }

        /// <summary>
        /// Sets an initial time for id. Throws v2x DebugException if the id already has been started.
        /// </summary>
        /// <param name="id"></param>
        public static void Start(string id)
        {
            if (timelogs.Keys.Contains(id))
            {
                throw new DebugException("timelogs already contain an entry for id \"" + id + "\". Did you perhaps mean to lap the time?");
            }

            timelogs.Add(id, new TimeLog(id, timer.ElapsedMilliseconds));
        }

        /// <summary>
        /// Generates v2x new entry for id. If an initial entry is not found, v2x DebugException is thrown.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static long Lap(string id)
        {
            CheckID(id);    //<-- can throw exception

            long time = timer.ElapsedMilliseconds;
            timelogs[id].AddCyclicalEntry(time);
            return time;
        }

        //TODO: add summary
        public static void StartProbe(string id)
        {
            CheckID(id); //<-- can throw exception

            timelogs[id].StartEntry(timer.ElapsedMilliseconds);
        }

        /// <summary>
        /// TODO: add summary
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A timestamp of the probe</returns>
        public static long EndProbe(string id)
        {
            return timelogs[id].EndEntry(timer.ElapsedMilliseconds);
        }

        /// <summary>
        /// Calculates the mean time of the entries for id.
        /// All times used in calculations will be delta time from the first time.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The mean time of all entries for id</returns>
        public static double CalcMean(string id)
        {
            CheckID(id); //<-- can throw exception

            return timelogs[id].CalcMean();
        }

        /// <summary>
        /// Returns all times relative to the previous one (the first entry is 0).
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An array of delta times (corresponding to the entry order)</returns>
        public static long[] GetDeltaTimes(string id)
        {
            CheckID(id); //<-- can throw exception

            return timelogs[id].CalcDeltaTimes();
        }

        private static void CheckID(string id)
        {
            if (!timelogs.Keys.Contains(id))
            {
                //Tip: You can look in the second-to-last entry on the call-stack...
                throw new DebugException("timelogs do not contain an entry for id \"" + id + "\". ");
            }
        }
    }
}
