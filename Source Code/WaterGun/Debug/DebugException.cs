﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Debug
{
    public class DebugException : Exception
    {
        public DebugException(string message) : base("[DBugException] \t " + message)
        {
            //do nothing (yet!) :P
        }
    }
}
