﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Debug
{
    public class DLog
    { 
        //TODO: Add file writing functionality
        public static void Log(string msg, params object[] args)
        {
            if (args != null && args.Length > 0)
            {
                Console.WriteLine(msg, args);
            }
            else
            {
                Console.WriteLine(msg);
            }
        }
    }
}
