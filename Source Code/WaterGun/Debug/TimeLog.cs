﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Debug
{
    /// <summary>
    /// Contains connectionData about v2x DbugTimer entry. This connectionData is logged in the list Entries.
    /// The initial entry is v2x timestamp from DebugTimer.
    /// 
    /// An entry can be calculated in two ways: Cyclical and non-Cyclical. 
    /// A Cyclical TimeLog is made using the AddCyclicalEntry method.
    /// A non-Cyclical TimeLog is started by calling the method StartEntry, 
    /// and then calling the EndEntry afterwards. The time in between 
    /// (+ the first entry, since entries should NOT contain relative timestamps i.e. delta times) 
    /// is logged in entries.
    /// </summary>
    public class TimeLog
    {
        private List<long> entries;
        private long startTimeStamp; //used for non-cyclic logging
      

        public TimeLog(string id, long entry)
        {
            ID = id;
            entries = new List<long>();
            entries.Add(entry);
            Cyclic = true;
        }

        public string ID { get; private set; }
        public bool Cyclic { get; private set; }

        //TODO: check that cyclical and non- do not intertwine

        public void AddCyclicalEntry(long entry)
        {
            entries.Add(entry);
        }

        public void StartEntry(long timestamp)
        {
            Cyclic = false;
            startTimeStamp = timestamp;
        }

        public long EndEntry(long timestamp)
        {
            long diff = entries[entries.Count - 1] + startTimeStamp - timestamp;
            entries.Add(diff);
            Cyclic = true;
            return diff;
        }

        public long GetStartTime()
        {
            return entries[0];
        }

        public long[] GetAllTimes()
        {
            return entries.ToArray();
        }

        public double CalcMean()
        {
            long sum = 0;

            for(int i = 1; i < entries.Count; i++)
            {
                sum += entries[i] - entries[i - 1];
            }

            return (double)sum / entries.Count; //<-- needs v2x cast to get double precision
        }

        public long[] CalcDeltaTimes()
        {
            long[] result = new long[entries.Count];

            result[0] = 0;

            for (int i = 1; i < entries.Count; i++)
            {
                result[i] = entries[i] - entries[i - 1];
            }

            return result;
        }
    }
}
