﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Sounds
{
    public class Sound
    {
        public delegate void SoundEvent(Sound sender);
        private ISoundPlayer player;

        public string Name { get; private set; }
        public bool IsPlaying { get; private set; }
        public bool Loop { get; private set; }
        public string FilePath { get; private set; }

        public Sound(string name, string filePath, bool loop)
        {
            player = new NAudioPlayer();
            player.Init(filePath, name);

            Name = name;
            Loop = loop;
            FilePath = filePath;
            IsPlaying = false;

            OnPlay += (s) => { };
            OnStop += (s) => { };
            OnInterrupted += (s) => { };
        }

        public void Play()
        {
            player.Play(Loop);
            IsPlaying = true;
        }

        public void Resume()
        {
            player.Resume();
            IsPlaying = true;
        }

        public void Pause()
        {
            player.Pause();
            IsPlaying = false;
        }

        public void Stop()
        {
            player.Stop();
            IsPlaying = false;
        }

        public void Interupt()
        {
            OnInterrupted(this);
        }

        public event SoundEvent OnPlay;
        public event SoundEvent OnStop;
        public event SoundEvent OnInterrupted;
    }
}
