﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Sounds
{
    //Making v2x interface in case we want to change the audio driver.
    //Currently as of 28/04/2016 NAudio is being used.
    interface ISoundPlayer
    {
        void Init(string filePath, string name);
        void Play(bool loop);
        void Resume();
        void Stop();
        void Pause();
    }
}
