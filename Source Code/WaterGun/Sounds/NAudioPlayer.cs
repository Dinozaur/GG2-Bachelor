﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WaterGun.Debug;

namespace WaterGun.Sounds
{
    class NAudioPlayer : ISoundPlayer
    {
        private string filePath;
        private string name;
        private WaveOut audioOutput;

        public bool KeepPlaying { get; set; }

        public NAudioPlayer()
        {
            audioOutput = null;
            filePath = "N/A";
        }

        public void Init(string filePath, string name)
        {
            //TODO Move to constructor?
            this.filePath = filePath;
            this.name = name;
        }

        public void Pause()
        {
            if(audioOutput != null)
            {
                audioOutput.Pause();
            }
            else
            {
                DLog.Log("Pausing audio failed, AudioOutput has not been set.");
            }
        }

        public void Play(bool loop)
        {
            //TODO: Refactor.
            new Thread(() =>
            {
                using (var wfr = new WaveFileReader(filePath))
                {
                    using (audioOutput = new WaveOut())
                    {
                        audioOutput.Init(wfr);
                        KeepPlaying = true;

                            audioOutput.Play();

                            while (audioOutput.PlaybackState != PlaybackState.Stopped && KeepPlaying)
                            {
                                Thread.Sleep(50);
                            }

                        audioOutput.Stop();
                    }
                }

                audioOutput = null;
            }).Start();
        }

        public void Resume()
        {
            if(audioOutput != null && audioOutput.PlaybackState == PlaybackState.Paused)
            {
                audioOutput.Play();
            }
            else
            {
                //TODO Use debug tools
                DLog.Log("Resuming audio failed");
            }
        }

        public void Stop()
        {
            if(audioOutput != null)
            {
                audioOutput.Stop();
            }
            else
            {
                //TODO Use debug tools
                DLog.Log("Stop audio failed");

            }
        }
    }
}
