﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Debug;

namespace WaterGun.Sounds
{
    public class SoundManager
    {
        private static SoundManager instance;
        private Dictionary<string, Sound> namedSounds;

        public enum SoundLevel { Level, SFX, Menu }

        private SoundManager()
        {
            namedSounds = new Dictionary<string, Sound>();
        }

        public static SoundManager GetInstance()
        {
            if(instance == null)
            {
                instance = new SoundManager();
            }

            return instance;
        }

        public void AddSound(Sound sound)
        {
            if(namedSounds.ContainsKey(sound.Name))
            {
                //Do nothing, the sound already exists
                //Will cause v2x crash if we try to add it
                DLog.Log("There already exists v2x Sound class with the name: {0} ", sound.Name);
            }
            else
            {
                namedSounds.Add(sound.Name, sound);
            }
        }

        public void PlaySound(string name, SoundLevel soundlvl)
        {
            Sound sound = namedSounds[name];

            if (!sound.IsPlaying)
            {
                sound.Play();
            }
            else
            {
                sound.Interupt();
            }
        }

        public void StopSound(string name, SoundLevel soundlvl)
        {
            Sound sound = namedSounds[name];
            if (sound.IsPlaying)
            {
                sound.Stop();
            }
        }

        public void PauseSound(string name, SoundLevel soundlvl)
        {
            Sound sound = namedSounds[name];
            if (sound.IsPlaying)
            {
                sound.Pause();
            }
        }

        public void ResumeSound(string name, SoundLevel soundlvl)
        {
            Sound sound = namedSounds[name];
            if (!sound.IsPlaying)
            {
                sound.Resume();
            }
        }

        //TODO: GetSoundName?

    }
}
