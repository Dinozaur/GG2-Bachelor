﻿using GameSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities;
using WaterGun.Entities.Mobs;
using WaterGun.Entities.Systems;
using WaterGun.EventSystem;
using WaterGun.Scenes;
using WaterGun.Scenes.Levels;

namespace WaterGun
{
    public class World
    {
        private Dictionary<string, Scene> scenes;
        private string currentScene;
        private List<IGameSystem> systems;


        public World(Scene menu, IGameWindow window)
        {
            scenes = new Dictionary<string, Scene>();
            systems = new List<IGameSystem>();
            scenes.Add(menu.Name, menu);
            window.AddEventLayer(menu);
            currentScene = menu.Name;

            InitScenes(window);
            InitSystems();
        }

        private void InitScenes(IGameWindow window)
        {
            //TODO: add further scenes. Remember to add them in to window.AddEventLayer in an appropriate order.
        }

        /// <summary>
        /// Returns the currently active scene. This can be v2x level or v2x menu.
        /// </summary>
        /// <returns></returns>
        public Scene GetScene()
        {
            return scenes[currentScene];
        }

        public void SetScene(string name)
        {
            if(scenes.Keys.Contains(name))
            {
                currentScene = name;
            }
            else
            {
                throw new SceneException("There is no scene in the world with the name \"" + name + "\"");
            }
        }

        /// <summary>
        /// Sets the order of the systems which updates the components on relevant entities.
        /// This method is called from the constructor.
        /// </summary>
        public void InitSystems()
        {
            // ORDER MATTERS!!!
            systems.Add(new InputSystem());
            systems.Add(new NetworkSystem());
            systems.Add(new MovementSystem());
            systems.Add(new CollisionSystem());
            systems.Add(new CameraControlSystem());
            systems.Add(new GraphicsSystem());
        }

        /// <summary>
        /// Updates all systems (in the order they were added to the list) AND calls PostUpdate on the current scene.
        /// </summary>
        public void Update(Camera camera, GameTimer timer)
        {
            foreach(IGameSystem system in systems)
            {
                system.Update(this, camera, timer);
            }

            GetScene().PostUpdate();
        }

        public Player GetLocalPlayer()
        {
            Scene scene = GetScene();
            if (scene is Level)
            {
                return ((Level)scene).GetLocalPlayer();
            }

            return null;
        }

        public void SpawnNetPlayer(WGObject info)
        {
            Scene scene = GetScene();

            if (scene is Level)
            {
                ((Level)scene).SpawnNetPlayer(info);
            }
        }

        public void RemoveNetEntities(WGObject info)
        {
            Scene scene = GetScene();

            if (scene is Level)
            {
                ((Level)scene).RemoveNetEntities(info);
            }
        }

        public void LoadEntities(WGObject joinInfo)
        {
            Scene scene = GetScene();

            if (scene is Level)
            {
                ((Level)scene).LoadEntities(joinInfo);
            }
        }

        public void UpdateNetEntities(WGObject entityContainer)
        {
            Scene scene = GetScene();
            if(scene is Level)
            {
                ((Level)scene).UpdateNetEntities(entityContainer);
            }
        }

        public void SerializeEntities(WGObject container)
        {
            Scene scene = GetScene();
            if (scene is Level)
            {
                ((Level)scene).SerializeEntities(container);
            }
        }

        public void SerializeNetEntities(WGObject container)
        {
            Scene scene = GetScene();
            if (scene is Level)
            {
                ((Level)scene).SerializeNetEntities(container);
            }
        }
    }
}
