﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Maths;

namespace WaterGun.Entities
{
    public class AnimationHandler
    {
        Dictionary<string, Animation> namedAnimations;
        public static readonly string DEFAULT = "DEFAULT";


        public AnimationHandler(string name, Animation anim)
        {
            namedAnimations = new Dictionary<string, Animation>();
            CurrentAnimation = name;

            namedAnimations.Add(DEFAULT, anim);
            namedAnimations.Add(name, anim);
        }

        public string CurrentAnimation { get; private set; }

        public void Update(int deltaTime)
        {
            namedAnimations[CurrentAnimation].Update(deltaTime);
        }

        public void AddAnimation(string name, Animation amin)
        {
            namedAnimations.Add(name, amin);
        }

        public void Change(string newAnim)
        {
            if(!CurrentAnimation.Equals(newAnim) && namedAnimations.ContainsKey(newAnim))
            {
                namedAnimations[CurrentAnimation].Reset();
                CurrentAnimation = newAnim;
            }
        }

        public Image GetImage()
        {
            return namedAnimations[CurrentAnimation].GetImage();
        }


        public void Rotate(Vector2d direction, bool flip)
        {
            namedAnimations[CurrentAnimation].Rotate(direction, flip);
        }
    }
}
