﻿using GameSerializer;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities.Components;
using WaterGun.Maths;

namespace WaterGun.Entities
{
    public class NetPlayer : Entity
    {
        public const string PREFIX = "NetPlayer";

        //TODO: fix diz!
        public NetPlayer(string name)
            : base(name, true)
        {
            this.GameEntityType = EntityType.PLAYER;
            LocationComponent lcomp = new LocationComponent(new Maths.Vector2d(0, 0));
            this.AddComponent(lcomp);

            PlayerComponent pcomp = new PlayerComponent();
            this.AddComponent(pcomp);

            Image img = new Bitmap(64,64);
            using(Graphics g = Graphics.FromImage(img)) {
                g.FillRectangle(new SolidBrush(Color.FromArgb(255, 0xff, 0x00, 0x00)), new Rectangle(0,0,img.Width, img.Height));
            }
            GraphicsComponent gcomp = new GraphicsComponent("idle", new Animation(img));
            this.AddComponent(gcomp);
        }

        public static NetPlayer Deserialize(WGObject info)
        {
            WGArray wgName = info.FindArray("Name");

            int pointer = 0;
            string name = SerializerUtil.ReadString(wgName.Data, ref pointer, wgName.Count);

            NetPlayer result = new NetPlayer(name);

            result.BaseDeserialize(info);

            return result;
        }

        public override void Update(WGObject wgoEntities)
        {
            WGObject location = wgoEntities.FindObject("Location");
            WGField locX = location.FindField("X");
            WGField locY = location.FindField("Y");

            WGObject aimDirection = wgoEntities.FindObject("AimDirection");
            WGField aimX = aimDirection.FindField("X");
            WGField aimY = aimDirection.FindField("Y");

            WGArray animation = wgoEntities.FindArray("Animation");
            
            int pointer = 0;
            string currentAnim = SerializerUtil.ReadString(animation.Data, ref pointer, animation.Count);

            pointer = 0;
            double lx = SerializerUtil.ReadDouble(locX.Data, ref pointer);
            pointer = 0;
            double ly = SerializerUtil.ReadDouble(locY.Data, ref pointer);
            pointer = 0;
            double ax = SerializerUtil.ReadDouble(aimX.Data, ref pointer);
            pointer = 0;
            double ay = SerializerUtil.ReadDouble(aimY.Data, ref pointer);

            GraphicsComponent gcomp = this.GetComponent<GraphicsComponent>();
            PlayerComponent pcomp = this.GetComponent<PlayerComponent>();
            LocationComponent lcomp = this.GetComponent<LocationComponent>();

            gcomp.AnimHandler.Change(currentAnim);
            pcomp.AimDirection = new Vector2d(ax, ay);
            lcomp.SetPoint(new Vector2d(lx, ly));
        }

        public override WGObject Serialize()
        {
            WGObject result = base.Serialize();
            return result;
        }

        public override string GetPrefix()
        {
            return PREFIX;
        }
    }
}
