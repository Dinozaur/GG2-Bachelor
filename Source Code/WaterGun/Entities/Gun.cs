﻿using GameSerializer;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities.Components;
using WaterGun.EventSystem;
using WaterGun.EventSystem.Types;
using WaterGun.Maths;
using WaterGun.Sounds;

namespace WaterGun.Entities
{
    public class Gun : Entity
    {
        public const string PREFIX = "Gun";

        public Gun(LocationComponent playerLocation, PlayerClass pc)
            : base(PREFIX, false)
        {
            Init(playerLocation, pc);
        }

        private Gun(LocationComponent netGunLocation, PlayerClass pc, string name)
            : base(name, true)
        {
            Init(netGunLocation, pc);
            
        }

        private void Init(LocationComponent playerLocation, PlayerClass pc)
        {
            this.GameEntityType = EntityType.GUN;
            this.AddComponent(playerLocation);

            this.AddComponent(new ClassComponent(pc));

            SizeComponent sizeComp = new SizeComponent(new Vector2d(50, 20));
            this.AddComponent(sizeComp);

            Animation idleAnim = null;
            Animation shootingAnim = null;
            Image ammo = null;


            switch (pc)
            {
                case PlayerClass.OVERWEIGHT:
                    idleAnim = new Animation(Image.FromFile(Directory.GetCurrentDirectory() +     "/Sprites/Weapons/MinigunS.images/image 0.png"));
                    shootingAnim = new Animation(Image.FromFile(Directory.GetCurrentDirectory() + "/Sprites/Weapons/Firing/MinigunFS.images/image 0.png"), 100);
                    shootingAnim.AddFrame(Image.FromFile(Directory.GetCurrentDirectory() +        "/Sprites/Weapons/Firing/MinigunFS.images/image 1.png"), 100);

                    ammo = Image.FromFile(Directory.GetCurrentDirectory() + "/Sprites/Projectiles/ShotS.images/image 0.png");
                    break;
            }
            GraphicsComponent gcomp = new GraphicsComponent("gun", idleAnim);
            gcomp.AddAnimation("shooting", shootingAnim);
           
            this.AddComponent(gcomp);

            GunComponent gunComp = new GunComponent(ammo);
            this.AddComponent(gunComp);

            LoadSFX();
        }

        public Entity Shoot()
        {
            GunComponent gunComp = this.GetComponent<GunComponent>();

            PlayGunSFX();

            gunComp.Cooldown = 200;

            //TODO: add some offset to the fireing
            double mag = 30;
            double dir = gunComp.AimDirection.InRadians();

            double dx = Math.Cos(dir) * mag;
            double dy = Math.Sin(dir) * mag + 10; //<-- add a bit since bullets have a size

            Vector2d fireOffset = new Vector2d(dx, dy);

            Bullet bullet = new Bullet(this.GetComponent<ClassComponent>().PlayerClass, this.GetCenter() + gunComp.RenderOffset + fireOffset, gunComp.AimDirection, 25);
            return bullet;
        }


        private void LoadSFX()
        {
            SoundManager manager = SoundManager.GetInstance();
            Sound gunSFX = new Sound("Gun", "Sounds/SFX/Gun.wav", true);
            manager.AddSound(gunSFX);
        }

        private void PlayGunSFX()
        {
            SoundManager manager = SoundManager.GetInstance();
            manager.PlaySound("Gun", SoundManager.SoundLevel.Level);
            manager.ResumeSound("Gun", SoundManager.SoundLevel.Level);
        }

        public static Gun Deserialize(WGObject wgo)
        {
            int pointer = 0;

            WGObject location =  wgo.FindObject("Location");
            double x = SerializerUtil.ReadDouble(location.FindField("X").Data, ref pointer);
            pointer = 0;
            double y = SerializerUtil.ReadDouble(location.FindField("Y").Data, ref pointer);
            pointer = 0;

            LocationComponent lcomp = new LocationComponent(new Vector2d(x, y));

            byte pc = SerializerUtil.ReadByte(wgo.FindField("Class").Data, ref pointer);
            pointer = 0;

            WGArray wgName = wgo.FindArray("Name");

            string name = SerializerUtil.ReadString(wgName.Data, ref pointer, wgName.Count);
            pointer = 0;

            Gun result = new Gun(lcomp, (PlayerClass)pc, name);
            result.BaseDeserialize(wgo);
            return result;
        }

        public override WGObject Serialize()
        {
            GraphicsComponent gcomp = this.GetComponent<GraphicsComponent>();
            LocationComponent lcomp = this.GetComponent<LocationComponent>();
            GunComponent guncomp = this.GetComponent<GunComponent>();

            WGObject result = base.Serialize();
            result.AddField(new WGField("Class", (byte)GetComponent<ClassComponent>().PlayerClass));

            WGArray animation = new WGArray("Animation", gcomp.AnimHandler.CurrentAnimation.ToCharArray());
            result.AddArray(animation);

            WGObject location = new WGObject("Location");
            location.AddField(new WGField("X", lcomp.Point.X));
            location.AddField(new WGField("Y", lcomp.Point.Y));
            result.AddObject(location);

            WGObject aimDirection = new WGObject("Aim");
            aimDirection.AddField(new WGField("X", guncomp.AimDirection.X));
            aimDirection.AddField(new WGField("Y", guncomp.AimDirection.Y));
            result.AddObject(aimDirection);

            return result;
        }

        public override void Update(WGObject wgoEntities)
        {
            GraphicsComponent gcomp = this.GetComponent<GraphicsComponent>();
            GunComponent guncomp = this.GetComponent<GunComponent>();
            LocationComponent lcomp = this.GetComponent<LocationComponent>();

            WGObject location = wgoEntities.FindObject("Location");
            WGField locX = location.FindField("X");
            WGField locY = location.FindField("Y");

            WGObject aimDirection = wgoEntities.FindObject("Aim");
            WGField aimX = aimDirection.FindField("X");
            WGField aimY = aimDirection.FindField("Y");

            WGArray animation = wgoEntities.FindArray("Animation");
            int pointer = 0;
            string currentAnim = SerializerUtil.ReadString(animation.Data, ref pointer, animation.Count);
            pointer = 0;
            double lx = SerializerUtil.ReadDouble(locX.Data, ref pointer);
            pointer = 0;
            double ly = SerializerUtil.ReadDouble(locY.Data, ref pointer);
            pointer = 0;
            double ax = SerializerUtil.ReadDouble(aimX.Data, ref pointer);
            pointer = 0;
            double ay = SerializerUtil.ReadDouble(aimY.Data, ref pointer);

            gcomp.AnimHandler.Change(currentAnim);
            lcomp.SetPoint(new Vector2d(lx, ly));
            guncomp.AimDirection = new Vector2d(ax, ay);
        }

        public override string GetPrefix()
        {
            return PREFIX;
        }
    }
}
