﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Entities.Systems
{
    public interface IGameSystem
    {
        //TODO: add Timer parameter
        void Update(World world, Camera camera, GameTimer timer);
    }
}
