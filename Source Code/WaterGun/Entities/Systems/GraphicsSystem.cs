﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities.Components;
using WaterGun.Entities.Mobs;
using WaterGun.Maths;

namespace WaterGun.Entities.Systems
{
    public class GraphicsSystem : IGameSystem
    {

        public void Update(World world, Camera camera, GameTimer timer)
        {
            List<Entity> entities = world.GetScene().GetEntities();

            foreach (Entity ent in entities)
            {

                if (ent.HasComponent<GraphicsComponent>())
                {
                    if(ent.HasComponent<MovementComponent>())
                    {
                        MovementComponent mcomp = ent.GetComponent<MovementComponent>();
                    }

                    GraphicsComponent gcomp = ent.GetComponent<GraphicsComponent>();

                    gcomp.Update(timer.DeltaTime);
                }

                if (ent is Gun) // TODO: make sensible check like: if (ent.HasComponent<GraphicsComponent>())
                {
                    GraphicsComponent gcomp = ent.GetComponent<GraphicsComponent>();
                    GunComponent gunComp = ent.GetComponent<GunComponent>();

                    double formattedDegrees = gunComp.AimDirection.InDegrees();

                    bool flip = formattedDegrees > 90 || formattedDegrees < -90;

                    double dir = flip ? 180 - formattedDegrees : formattedDegrees;
                    Vector2d direction = new Vector2d(Math.Cos(dir * Math.PI / 180), Math.Sin(dir * Math.PI / 180));

                    gcomp.RotateImage(direction, flip);
                }

                else if (ent is Bullet)
                {
                    GraphicsComponent gcomp = ent.GetComponent<GraphicsComponent>();
                    MovementComponent mcomp = ent.GetComponent<MovementComponent>();

                    gcomp.RotateImage(mcomp.Movement, false);
                }
                else if (ent is Player)
                {
                    GraphicsComponent gcomp = ent.GetComponent<GraphicsComponent>();
                    PlayerComponent pcomp = ent.GetComponent<PlayerComponent>();

                    double formattedDegrees = pcomp.AimDirection.InDegrees();

                    bool flip = formattedDegrees > 90 || formattedDegrees < -90;

                    gcomp.RotateImage(new Vector2d(1,0), flip);
                }

            }
        }
    }
}
