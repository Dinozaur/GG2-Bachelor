﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Debug;
using WaterGun.Entities.Components;
using WaterGun.Entities.Mobs;
using WaterGun.Maths;

namespace WaterGun.Entities.Systems
{

    public class InputSystem : IGameSystem
    {

        public void Update(World world, Camera camera, GameTimer timer)
        {
            List<Entity> entities = world.GetScene().GetEntities();

            foreach (Entity ent in entities)
            {
                //TODO refactor to make it purdy:

                if (ent is Gun)
                {
                    GraphicsComponent gcomp = ent.GetComponent<GraphicsComponent>();
                    GunComponent gunComp = ent.GetComponent<GunComponent>();
                    LocationComponent lcomp = ent.GetComponent<LocationComponent>();
                    SizeComponent scomp = ent.GetComponent<SizeComponent>();

                    //DLog.Log("Updating");
                    gunComp.Update(timer.DeltaTime);

                    Vector2d screenPos = new Vector2d((lcomp.Point.X - camera.X) + scomp.Size.X / 2, (lcomp.Point.Y - camera.Y) + scomp.Size.Y / 2);

                    Vector2d mPos = gunComp.MousePosition;

                    Vector2d rotationDirection = new Vector2d(mPos.X - screenPos.X, mPos.Y - screenPos.Y);

                    gunComp.AimDirection = rotationDirection;

                    //gcomp.RotateImage(rotationDirection); <-- moved to GraphicsSystem

                    if (gunComp.IsShooting && gunComp.Cooldown <= 0)
                    {
                        Entity bullet = ((Gun)ent).Shoot();
                        bullet.GetComponent<BulletComponent>().OnMaxDistanceReached += (sender) =>
                        {
                            world.GetScene().RemoveEntity(sender);
                        };

                        CollisionComponent ccomp = bullet.GetComponent<CollisionComponent>();
                        ccomp.OnColide += (sender, other, intersectionSize) =>
                        {
                            Bullet.OnCollide(world.GetScene(), sender, other, intersectionSize);
                        };

                        world.GetScene().AddEntity(bullet);    
                    }
                }
                else if (ent is Player)
                {
                    GraphicsComponent gcomp = ent.GetComponent<GraphicsComponent>();
                    PlayerComponent pcomp = ent.GetComponent<PlayerComponent>();
                    LocationComponent lcomp = ent.GetComponent<LocationComponent>();
                    SizeComponent scomp = ent.GetComponent<SizeComponent>();

                    Vector2d screenPos = new Vector2d((lcomp.Point.X - camera.X) + scomp.Size.X / 2, (lcomp.Point.Y - camera.Y) + scomp.Size.Y / 2);

                    Vector2d mPos = pcomp.MousePosition;

                    pcomp.AimDirection = mPos - screenPos;
                }
            }
            

        }
    }
}
