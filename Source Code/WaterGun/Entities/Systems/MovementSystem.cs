﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities.Components;
using WaterGun.Entities.Mobs;
using WaterGun.Maths;

namespace WaterGun.Entities.Systems
{
    class MovementSystem: IGameSystem
    {
        public void Update(World world, Camera camera, GameTimer timer)
        {
            List<Entity> entities = world.GetScene().GetEntities();

            foreach (Entity entity in entities)
            {
                
                if (entity.HasComponent<MovementComponent>() && entity.HasComponent<LocationComponent>())
                {
                    if ((!entity.HasComponent<NetworkComponent>()) || (entity.HasComponent<NetworkComponent>() && !entity.GetComponent<NetworkComponent>().HasBeenUpdated))
                    {
                        LocationComponent lcomp = entity.GetComponent<LocationComponent>();
                        MovementComponent mcomp = entity.GetComponent<MovementComponent>();

                        if (entity is Player)
                        {
                            PlayerComponent pcomp = entity.GetComponent<PlayerComponent>();

                            if (pcomp.Left)
                            {
                                mcomp.AddVector(new Vector2d(-mcomp.Acceleration, 0));
                            }

                            if (pcomp.Right)
                            {
                                mcomp.AddVector(new Vector2d(mcomp.Acceleration, 0));
                            }

                            if (!pcomp.Right && !pcomp.Left)
                            {
                                double magnitude = mcomp.Friction;
                                double direction = mcomp.Movement.InRadians();

                                double dx = Math.Cos(direction) * magnitude;
                                double dy = Math.Sin(direction) * magnitude;

                                mcomp.SubtractVector(new Vector2d(dx, dy));
                            }

                            if (pcomp.Jump)
                            {
                                mcomp.Jump(jumpHeight: 40);
                            }
                        }

                        if (entity is Bullet)
                        {
                            string d = "bug";
                        }

                        if (entity.HasComponent<GravityComponent>())
                        {
                            GravityComponent gravComp = entity.GetComponent<GravityComponent>();
                            gravComp.Update(timer.TimeFactor);

                            if (mcomp.Movement.Y < gravComp.MaxPull)
                            {
                                mcomp.AddVector(gravComp.Pull);
                            }
                        }

                        //TODO: test if this works
                        Vector2d distance = new Vector2d(0, 0);
                        distance += lcomp.Point;
                        lcomp.ApplyVector(mcomp.Movement, timer.TimeFactor);
                        distance = lcomp.Point - distance;

                        if (entity.HasComponent<BulletComponent>())
                        {
                            BulletComponent bcomp = entity.GetComponent<BulletComponent>();
                            bcomp.Update(entity, distance.CalcMagnitude());
                        }
                    }

                }
            }
        }
    }
}
