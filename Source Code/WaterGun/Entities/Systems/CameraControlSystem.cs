﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities.Components;
using WaterGun.Entities.Mobs;

namespace WaterGun.Entities.Systems
{
    public class CameraControlSystem : IGameSystem
    {
        public void Update(World world, Camera camera, GameTimer timer)
        {
            List<Entity> entities = world.GetScene().GetEntities();

            foreach (Entity ent in entities)
            {
                if (ent is Player)
                {
                    CameraControlComponent cccomp = ent.GetComponent<CameraControlComponent>();

                    if (cccomp.ControllingEntity.Equals(ent))
                    {
                        camera.SetCenter(ent.GetCenter());
                    }
                }
            }
        }
    }
}
