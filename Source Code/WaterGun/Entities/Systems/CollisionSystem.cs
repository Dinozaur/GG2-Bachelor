﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities.Components;
using WaterGun.Maths;

namespace WaterGun.Entities.Systems
{
    //It's rather difficult to test this with v2x unit test, we will most likely have to resort to manual testing for Collision testing.
    class CollisionSystem: IGameSystem
    {
        public void Update(World world, Camera camera, GameTimer timer)
        {
            //Get all entities that are in the current level.
            List<Entity> entities = world.GetScene().GetEntities();

            //We want to keep track of which entities have been checked.
            Dictionary<Entity, Rectangle> checkedEntityBounds = new Dictionary<Entity, Rectangle>();

            //Check all entities that has v2x collision component.
            foreach(Entity ent in entities)
            {
                if(ent.HasComponent<CollisionComponent>())
                {
                    //We need information from other components to determine how big the collision box should be for the chosen entity
                    LocationComponent lcomp = ent.GetComponent<LocationComponent>();
                    SizeComponent scomp = ent.GetComponent<SizeComponent>();
                    CollisionComponent ccomp = ent.GetComponent<CollisionComponent>();

                    //We have v2x Rectangle from Drawing as v2x collision hitbox.
                    Rectangle entityHitbox = new Rectangle((int)(lcomp.Point.X + ccomp.X), (int)(lcomp.Point.Y + ccomp.Y), (int)scomp.Size.X, (int)scomp.Size.Y);
                    foreach(Entity other in checkedEntityBounds.Keys)
                    {
                        //Get the hitbox of the other Entity
                        Rectangle otherEntityHitbox = checkedEntityBounds[other];

                        //If the hitbox collides with something, call the collision event on CollisionComponent.
                        if(entityHitbox.IntersectsWith(otherEntityHitbox))
                        {
                            Rectangle intersection = new Rectangle(entityHitbox.X, entityHitbox.Y, entityHitbox.Width, entityHitbox.Height);

                            intersection.Intersect(otherEntityHitbox);

                            Vector2d intersectionSize = new Vector2d(intersection.Width, intersection.Height);

                            ccomp.Collide(ent, other, intersectionSize);
                            other.GetComponent<CollisionComponent>().Collide(other, ent, intersectionSize);
                        }
                    }
                    // Entity has been checked, add it to the dictionary.
                    checkedEntityBounds.Add(ent, entityHitbox);
                }
            }
        }
    }
}
