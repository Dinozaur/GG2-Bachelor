﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities.Components;

namespace WaterGun.Entities.Systems
{
    public class NetworkSystem : IGameSystem
    {
        public void Update(World world, Camera camera, GameTimer timer)
        {
            List<Entity> entities = world.GetScene().GetEntities();

            foreach(Entity ent in entities)
            {
                if(ent.HasComponent<NetworkComponent>())
                {
                    ent.GetComponent<NetworkComponent>().HasBeenUpdated = false;
                }
            }
        }
    }
}
