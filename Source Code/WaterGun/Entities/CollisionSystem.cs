﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Entities.Components
{
    //It's rather difficult to test this with a unit test, we will most likely have to resort to manual testing for Collision testing.
    class CollisionSystem: IGameSystem
    {
        public void Update(World world)
        {
            //Get all entities that are in the current level.
            List<Entity> entities = world.GetScene().GetEntities();

            //We want to keep track of which entities have been checked.
            Dictionary<Entity, Rectangle> checkedEntityBounds = new Dictionary<Entity, Rectangle>();

            //Check all entities that has a collision component.
            foreach(Entity ent in entities)
            {
                if(ent.HasComponent<CollisionComponent>())
                {
                    //We need information from other components to determine how big the collision box should be for the chosen entity
                    LocationComponent lcomp = ent.GetComponent<LocationComponent>();
                    SizeComponent scomp = ent.GetComponent<SizeComponent>();
                    CollisionComponent ccomp = ent.GetComponent<CollisionComponent>();

                    //We have a Rectangle from Drawing as a collision hitbox.
                    Rectangle entityHitbox = new Rectangle((int)(lcomp.Point.X - ccomp.X), (int)(lcomp.Point.Y - ccomp.Y), (int)scomp.Size.X, (int)scomp.Size.Y);
                    foreach(Entity other in checkedEntityBounds.Keys)
                    {
                        //Get the hitbox of the other Entity
                        Rectangle otherEntityHitbox = checkedEntityBounds[other];

                        //If the hitbox collides with something, call the collision event on CollisionComponent.
                        if(entityHitbox.IntersectsWith(otherEntityHitbox))
                        {
                            ccomp.Collide(ent, other);
                            other.GetComponent<CollisionComponent>().Collide(other, ent);
                        }
                    }
                    // Entity has been checked, add it to the dictionary.
                    checkedEntityBounds.Add(ent, entityHitbox);
                }
            }
        }
    }
}
