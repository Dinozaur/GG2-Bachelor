﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities.Components;
using WaterGun.Maths;

namespace WaterGun.Entities
{
    class MovementSystem: IGameSystem
    {
        public void Update(World world)
        {
            List<Entity> entities = world.GetScene().GetEntities();

            foreach (Entity entity in entities)
            {
                if (entity.HasComponent<MovementComponent>() && entity.HasComponent<LocationComponent>())
                {
                    LocationComponent lcomp = entity.GetComponent<LocationComponent>();
                    MovementComponent mcomp = entity.GetComponent<MovementComponent>();

                    double d = mcomp.Speed;

                    lcomp = new LocationComponent(new Vector2d(d += lcomp.Point.X, d += lcomp.Point.Y));

                }
            }
        }
    }
}
