﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameSerializer;
using WaterGun.Entities.Components;
using WaterGun.Maths;
using System.IO;
using WaterGun.Scenes.Levels;
using WaterGun.Scenes;
using WaterGun.Entities.Mobs;

namespace WaterGun.Entities
{
    public class Bullet : Entity
    {
        public const string PREFIX = "Bullet";

        private readonly double SPEED = 20;
        private readonly double GRAVITY_PULL = 0.5;
        private readonly double MAX_GRAVITY = 5.0;
        private readonly double MAX_DISTANCE = 500;
        private static readonly int DAMAGE = 10;

        public Bullet(PlayerClass pc, Vector2d location, Vector2d direction, double sprayMarginInDegrees)
            : base(PREFIX, false)
        {
            Init(pc, location, direction, sprayMarginInDegrees);
        }

        /// <summary>
        /// Private constructor which is used for Deserialization. 
        /// </summary>
        private Bullet(PlayerClass pc, Vector2d location, Vector2d direction, string name): base(name, true)
        {
            Init(pc, location, direction, 0);
        }

        private void Init(PlayerClass pc, Vector2d location, Vector2d direction, double sprayMarginInDegrees)
        {
            this.GameEntityType = EntityType.BULLET;
            Image sprite = null;

            switch (pc)
            {
                case PlayerClass.OVERWEIGHT:
                    sprite = Image.FromFile(Directory.GetCurrentDirectory() + "/Sprites/Projectiles/ShotS.images/image 0.png");
                    break;
            }

            LocationComponent lcomp = new LocationComponent(location);
            this.AddComponent(lcomp);

            //Legacy Code
            //double degrees = direction.InDegrees();
            //degrees = degrees % 360;
            //degrees += 90;
            //if (degrees < 180)
            //{
            //    flip = true;
            //}

            Animation anim = new Animation(sprite);

            GraphicsComponent gcomp = new GraphicsComponent("bullet", anim);
            this.AddComponent(gcomp);
            //flip doesn't matter in RotateImage, rotate handles it.
            gcomp.RotateImage(direction, false);

            CollisionComponent ccomp = new CollisionComponent(false);
            this.AddComponent(ccomp);

            double dir = direction.InDegrees() + Game.Dice.Next((int)(sprayMarginInDegrees / 2)) - (sprayMarginInDegrees / 2);

            double dx = Math.Cos(dir * Math.PI / 180) * SPEED;
            double dy = Math.Sin(dir * Math.PI / 180) * SPEED;

            this.AddComponent(new SizeComponent(new Vector2d(sprite.Width, sprite.Height)));

            MovementComponent mcomp = new MovementComponent(new Vector2d(dx, dy), 50, 1.5, 0);
            this.AddComponent(mcomp);

            this.AddComponent(new GravityComponent(GRAVITY_PULL, MAX_GRAVITY));
            this.AddComponent(new BulletComponent(MAX_DISTANCE));
        }

        public static Bullet Deserialize(WGObject wgo)
        {
            WGField bulletClass = wgo.FindField("Class");
            WGArray name = wgo.FindArray("Name");

            WGObject direction = wgo.FindObject("Direction");
            WGField dirX = direction.FindField("X");
            WGField dirY = direction.FindField("Y");

            WGObject location = wgo.FindObject("Location");
            WGField locX = location.FindField("X");
            WGField locY = location.FindField("Y");

            WGObject distance = wgo.FindObject("Distance");
            WGField distTotal = distance.FindField("Total");
            WGField distRemaining = distance.FindField("Remaining");

            int pointer = 0;
      
            double lx = SerializerUtil.ReadDouble(locX.Data, ref pointer);
            pointer = 0;
            double ly = SerializerUtil.ReadDouble(locY.Data, ref pointer);
            pointer = 0;
            double dx = SerializerUtil.ReadDouble(dirX.Data, ref pointer);
            pointer = 0;
            double dy = SerializerUtil.ReadDouble(dirY.Data, ref pointer);
            pointer = 0;
            byte bc = SerializerUtil.ReadByte(bulletClass.Data, ref pointer);
            pointer = 0;
            string n = SerializerUtil.ReadString(name.Data, ref pointer, name.Count);
            pointer = 0;
            double dTotal = SerializerUtil.ReadDouble(distTotal.Data, ref pointer);
            pointer = 0;
            double dRemaining = SerializerUtil.ReadDouble(distRemaining.Data, ref pointer);

            Bullet result = new Bullet((PlayerClass)bc, new Vector2d(lx, ly), new Vector2d(dx, dy), n);
            result.BaseDeserialize(wgo);
            BulletComponent bcomp = result.GetComponent<BulletComponent>();
            bcomp.DistanceLeft = dRemaining;
            bcomp.TotalDistance = dTotal;
            return result;
        }

        public static void OnCollide(Scene scene, Entity sender, Entity other, Vector2d intersectionSize)
        {
            if (other is Wall)
            {
                scene.RemoveEntity(sender);
            }
            else if (other is Player)
            {
                PlayerComponent pcomp = other.GetComponent<PlayerComponent>();
                Player player = (Player)other;

                pcomp.TakeDamage(DAMAGE);

                if (pcomp.IsDead())
                {
                    scene.RemoveEntity(sender);
                    Level level = (Level)scene;
                    level.SpawnPlayer(player, SpawnComponent.TeamColor.RED);
                    pcomp.ResetHealth();
                }
            }
        }

        public override WGObject Serialize()
        {
            MovementComponent mcomp = this.GetComponent<MovementComponent>();
            LocationComponent lcomp = this.GetComponent<LocationComponent>();
            BulletComponent bcomp = this.GetComponent<BulletComponent>();

            WGObject result = base.Serialize();
            result.AddField(new WGField("Class", (byte)PlayerClass.OVERWEIGHT));

            WGObject direction = new WGObject("Direction");
            direction.AddField(new WGField("X", mcomp.Movement.X));
            direction.AddField(new WGField("Y", mcomp.Movement.Y));
            result.AddObject(direction);

            WGObject location = new WGObject("Location");
            location.AddField(new WGField("X", lcomp.Point.X));
            location.AddField(new WGField("Y", lcomp.Point.Y));
            result.AddObject(location);


            WGObject distance = new WGObject("Distance");
            distance.AddField(new WGField("Total", bcomp.TotalDistance));
            distance.AddField(new WGField("Remaining", bcomp.DistanceLeft));
            result.AddObject(distance);

            return result;
        }

        public override void Update(WGObject wgo)
        {
            MovementComponent mcomp = this.GetComponent<MovementComponent>();
            LocationComponent lcomp = this.GetComponent<LocationComponent>();
            BulletComponent bcomp = this.GetComponent<BulletComponent>();

            WGObject direction = wgo.FindObject("Direction");
            WGField dirX = direction.FindField("X");
            WGField dirY = direction.FindField("Y");

            WGObject location = wgo.FindObject("Location");
            WGField locX = location.FindField("X");
            WGField locY = location.FindField("Y");

            WGObject distance = wgo.FindObject("Distance");
            WGField distTotal = distance.FindField("Total");
            WGField distRemaining = distance.FindField("Remaining");

            int pointer = 0;
            double lx = SerializerUtil.ReadDouble(locX.Data, ref pointer);
            pointer = 0;
            double ly = SerializerUtil.ReadDouble(locY.Data, ref pointer);
            pointer = 0;
            double dx = SerializerUtil.ReadDouble(dirX.Data, ref pointer);
            pointer = 0;
            double dy = SerializerUtil.ReadDouble(dirY.Data, ref pointer);
            pointer = 0;
            double dTotal = SerializerUtil.ReadDouble(distTotal.Data, ref pointer);
            pointer = 0;
            double dRemaining = SerializerUtil.ReadDouble(distRemaining.Data, ref pointer);


            lcomp.SetPoint(new Vector2d(lx, ly));
            mcomp.Movement = new Vector2d(dx, dy);
            bcomp.TotalDistance = dTotal;
            bcomp.DistanceLeft = dRemaining;
        }

        public override string GetPrefix()
        {
            return PREFIX;
        }
    }
}
