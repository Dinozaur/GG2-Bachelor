﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameSerializer;
using WaterGun.Debug;
using WaterGun.Entities.Components;
using WaterGun.Maths;

namespace WaterGun.Entities
{
    public class Wall : Entity
    {
        public const string PREFIX = "Wall";
        public Wall(Vector2d location, Vector2d size)
            : base(PREFIX, false)
        {
            this.GameEntityType = EntityType.WALL;
            this.AddComponent(new LocationComponent(location));
            this.AddComponent(new SizeComponent(size));
            CollisionComponent ccomp = new CollisionComponent(true);
            ccomp.OnColide += (sender, other, i) =>
            {
                //DLog.Log(sender + " is colliding with: " + other);
            };
            this.AddComponent(ccomp);
        }

        public override string GetPrefix()
        {
            return PREFIX;
        }

        public override WGObject Serialize()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Hopefully this will never be called
        /// Walls should NEVER call update.
        /// </summary>
        /// <param name="wgoEntities"></param>
        public override void Update(WGObject wgoEntities)
        {
            //TODO: Remove this
            //throw new NotImplementedException();
        }
    }
}
