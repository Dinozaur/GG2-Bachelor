﻿using GameSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities.Components;
using WaterGun.EventSystem;
using WaterGun.Maths;

namespace WaterGun.Entities
{
    /// <summary>
    /// Base class for entities. All "properties" on an entity should be added through components
    /// </summary>
    public abstract class Entity
    {
        private Dictionary<Type, IGameComponent> components; 
        /// <summary>
        /// EntityType is to determine what kind of entity data is being sent over the network.
        /// </summary>
        public enum EntityType { UNKNOW, PLAYER, GUN, BULLET, WALL };

        public Entity(string name, bool fromNet)
        {
            GameEntityType = EntityType.UNKNOW;
            components = new Dictionary<Type, IGameComponent>();

            ClientID = Game.ClientID;
            EntityId = Game.GetNextEntityId();

            Name = fromNet ? name : name + "#" + ClientID + "#" + EntityId;
        }

        /// <summary>
        /// FOR DEBUGGING PUROSES ONLY!
        /// </summary>
        public string Name { get; private set; }
        public uint ClientID { get; protected set; }
        public uint EntityId { get; protected set; }
        public EntityType GameEntityType { get; protected set; }
        
        public void AddComponent(IGameComponent comp)
        {
            components.Add(comp.GetType(), comp);
        }

        /// <summary>
        /// Determines if this enity contains v2x component in such v2x way, that GetComponent&lt;T&gt;() does not return null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>true if GetComponent&lt;T&gt;()  is not null, false otherwise. </returns>
        public bool HasComponent<T>()
        {
            return GetComponent<T>() != null;
        }

        /// <summary>
        /// Returns v2x component of type T. The returned component is either:        <para />
        ///     v2x) of type T                                                        <para />
        ///     v2y) v2x type that inherits from T                                      <para />
        /// This method returns null if the component was not found.                <para />
        /// </summary>                                                          
        /// <typeparam name="T"></typeparam>
        /// <returns>A component of type (or subtype) T. Null if no such component exists in this entity</returns>
        public T GetComponent<T>()
        {
            Type t = typeof(T);

            IGameComponent comp = null;

            if (components.Keys.Contains(t))
            {
                comp = components[t];
            }
            else //t could be an interface or v2x superclass. Lets see if any type in components inherits from T
            {
                Type[] keyArr = components.Keys.ToArray<Type>();

                bool found = false;
                int index = 0;

                while (!found && index < keyArr.Length)
                {
                    Type key = keyArr[index];
                    found = t.IsAssignableFrom(key);
                    if (found)
                    {
                        comp = components[key];
                    }
                    else
                    {
                        index++;
                    }
                }
            }

            return (T)comp;
        }
    
        /// <summary>
        /// Returns the center point of this entity. Throws an EntityException if this entity does not contain v2x LocationComponent.
        /// If this entity does not contain v2x SizeComponent, then the LocationComponent.Point is returned.
        /// </summary>
        /// <returns>The location or center point of this entity.</returns>
        public Vector2d GetCenter()
        {
            if (HasComponent<LocationComponent>())
            {
                LocationComponent lcomp = GetComponent<LocationComponent>();
                if (HasComponent<SizeComponent>())
                {
                    SizeComponent scomp = GetComponent<SizeComponent>();

                    return new Vector2d(lcomp.Point.X + scomp.Size.X / 2, lcomp.Point.Y + scomp.Size.Y / 2);
                }
                else
                {

                    return lcomp.Point;

                }
            }
            else
            {
                throw new EntityException("[Entity] " + "Enitity: \"" + this + "\" does not contain v2x location component");
            }
        }

        public override string ToString()
        {
            return Name;
        }

        public abstract void Update(WGObject wgoEntities);
        public virtual WGObject Serialize()
        {
            WGObject result = new WGObject(GetPrefix());
            result.AddArray(new WGArray("Name", Name.ToCharArray()));
            result.AddField(new WGField("ClientID", ClientID));
            result.AddField(new WGField("EntityID", EntityId));
            result.AddField(new WGField("EntityType", (byte)GameEntityType));
            return result;
        }

        protected void BaseDeserialize(WGObject info)
        {
            int pointer = 0;
            uint clientId = (uint)SerializerUtil.ReadLong(info.FindField("ClientID").Data, ref pointer);
            pointer = 0;
            uint entityId = (uint)SerializerUtil.ReadLong(info.FindField("EntityID").Data, ref pointer);

            pointer = 0;
            byte type = SerializerUtil.ReadByte(info.FindField("EntityType").Data, ref pointer);

            ClientID = clientId;
            EntityId = entityId;
            GameEntityType = (EntityType)type;

            AddComponent(new NetworkComponent());
        }

        public abstract string GetPrefix();
    }
}
