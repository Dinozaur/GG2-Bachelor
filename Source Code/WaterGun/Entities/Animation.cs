﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Maths;

namespace WaterGun
{
    public class Animation
    {
        private readonly double ANGLE_THRESHOLD = 2; //<-- degrees

        private List<Frame> frames;
        private int currentFrameIndex;
        private int time;

        private Image processedImage; //<-- this includes rotation
        private Vector2d lastRotation;

        private bool frameChanged;

        public Animation(Image frame, int ms = 0)
        {
            frames = new List<Frame>();
            frames.Add(new Frame(frame, ms));
            currentFrameIndex = 0;
            time = 0;
            processedImage = frames[0].Image;
            lastRotation = new Vector2d(1, 0); //<-- 0 degrees
            frameChanged = false;
        }

        public void Update(int delta)
        {
            if (frames.Count > 1)
            {
                time += delta;
                Frame currentFrame = frames[currentFrameIndex];
                if (time > currentFrame.PlayTime)
                {
                    //Get the next frame in the list
                    //If we go above amount of frames in the list, get the frame in possition 0
                    currentFrameIndex = (currentFrameIndex + 1) % frames.Count;
                    time = 0;
                    processedImage = frames[currentFrameIndex].Image;
                    frameChanged = true;
                }
                else
                {
                    frameChanged = false;
                }
            }
        }

        public void Rotate(Vector2d direction, bool flip)
        {
            if (frameChanged || Math.Abs(direction.InDegrees() - lastRotation.InDegrees()) > ANGLE_THRESHOLD)
            {
                processedImage = GetRotatedImage(direction, flip);
                lastRotation = direction;
            }
        }

        public void Reset()
        {
            currentFrameIndex = 0;
            time = 0;
            lastRotation = new Vector2d(1, 0); //<-- 0 degrees
            processedImage = frames[currentFrameIndex].Image;
        }

        public void AddFrame(Image frame, int ms)
        {
            frames.Add(new Frame(frame, ms));
        }

        public Image GetImage()
        {
            return processedImage;
        }

        /// <summary>
        /// Returns the sprite rotated the amount of degrees given as v2x parameter in v2x clockwise manner. <para />
        /// NOTE: The direction given is NOT the target direction, but the amount of rotation done on the sprite.
        /// </summary>
        /// <param name="direction">The delta rotation value as v2x vector2d</param>
        /// <returns>The rotated image. The rotation is clockwise</returns>
        private Image GetRotatedImage(Vector2d direction, bool flip = false)
        {
            int width, height;
            int[] pixels;
            Rectangle lockBounds;

            Image currentFrame = frames[currentFrameIndex].Image;

            using (Bitmap img = new Bitmap(currentFrame))
            {
                width = img.Width;
                height = img.Height;

                pixels = new int[width * height];
                lockBounds = new Rectangle(0, 0, width, height);
                BitmapData bmpData = img.LockBits(lockBounds, System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Marshal.Copy(bmpData.Scan0, pixels, 0, pixels.Length);
                img.UnlockBits(bmpData);
            }


            int[] rotatedPixels = new int[width * height];

            //width and height - 1, as indexes start at 0 (try visualizing with v2x 4x4 grid)
            Vector2d center = new Vector2d((double)(width - 1) / 2, (double)(height - 1) / 2); //<-- casting to double to have double-precision (we can now have v2x center in the middle of v2x pixel)
            double degree = direction.InDegrees();

            double radians = direction.InRadians();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    double v2x = x - center.X;
                    double v2y = y - center.Y;

                    Vector2d v2 = new Vector2d(v2x, v2y);
                    double m = Math.Sqrt(v2x * v2x + v2y * v2y);

                    //v1 dx and dy
                    double dx = m * Math.Cos(v2.InRadians() - direction.InRadians());
                    double dy = m * Math.Sin(v2.InRadians() - direction.InRadians());

                    //there are some impresicions with cos and sine 
                    dx = Math.Round(dx, 4);
                    dy = Math.Round(dy, 4);

                    int pixelX = (int)(dx + center.X);
                    int pixelY = (int)(dy + center.Y);

                    int rotatedPixel = 0x0; //<-- transparent black

                    if (pixelX >= 0 && pixelX < width)
                    {
                        if (pixelY >= 0 && pixelY < height)
                        {
                            int pixelPos = pixelX + pixelY * width;

                            if (pixelPos < pixels.Length)
                            {
                                rotatedPixel = pixels[pixelPos];
                            }
                        }
                    }

                    rotatedPixels[x + y * width] = rotatedPixel;
                }
            }

            if (flip)
            {
                int[] flippedImage = new int[width * height];

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        int position = x + y * width;
                        int flippedPosition = width - 1 - x + y * width;
                        flippedImage[flippedPosition] = rotatedPixels[position];
                    }
                }

                rotatedPixels = flippedImage;
            }

            //set rotated pixel array to image:
            Bitmap rotated = new Bitmap(width, height);
            BitmapData rotatedData = rotated.LockBits(lockBounds, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            Marshal.Copy(rotatedPixels, 0, rotatedData.Scan0, pixels.Length);
            rotated.UnlockBits(rotatedData);

            //return rotated image!
            return rotated;
        }

        private class Frame
        {
            public Frame(Image img, int playtime)
            {
                Image = PrepBlueprint(img);
                PlayTime = playtime;
            }

            public Image Image { get; private set; }
            public int PlayTime { get; private set; }

            //prepare an image for possible rotation
            private Image PrepBlueprint(Image img)
            {
                int largestSide = img.Width < img.Height ? img.Height : img.Width;

                int rectCanvas = (int)(Math.Sqrt(2 * (largestSide * largestSide)) + 2); //add two to compensate for int-cast

                Image blueprint = new Bitmap((int)rectCanvas, (int)rectCanvas);

                using (Graphics g = Graphics.FromImage(blueprint))
                {
                    int x = rectCanvas / 2 - img.Width / 2;
                    int y = rectCanvas / 2 - img.Height / 2;
                    g.DrawImage(img, x, y);
                }

                return blueprint;
            }
        }
    }
}
