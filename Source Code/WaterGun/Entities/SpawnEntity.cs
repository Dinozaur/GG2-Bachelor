﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameSerializer;
using WaterGun.Entities.Components;
using WaterGun.Maths;

namespace WaterGun.Entities
{
    public class SpawnEntity : Entity
    {
        public const string PREFIX = "SpawnEntity";
        public SpawnEntity(Vector2d location, SpawnComponent.TeamColor team)
            : base(PREFIX, false)
        {
            LocationComponent lcomp = new LocationComponent(location);
            this.AddComponent(lcomp);

            SpawnComponent scomp = new SpawnComponent(team);
        }

        public override string GetPrefix()
        {
            return PREFIX;
        }

        public override WGObject Serialize()
        {
            throw new NotImplementedException();
        }

        public override void Update(WGObject wgoEntities)
        {
            throw new NotImplementedException();
        }
    }
}
