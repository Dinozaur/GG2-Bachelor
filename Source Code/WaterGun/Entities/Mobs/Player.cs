﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Debug;
using WaterGun.Entities.Components;
using WaterGun.EventSystem;
using WaterGun.EventSystem.Types;
using WaterGun.Maths;
using System.Windows.Forms;
using WaterGun.Sprites;
using System.IO;
using GameSerializer;

namespace WaterGun.Entities.Mobs
{

    public class Player : Entity
    {
        public const string PREFIX = "Player";

        public Player(uint clientId, Vector2d location, PlayerClass pc) : base(PREFIX, false)
        {
            Init(location, pc);
        }
        private Player(string name, Vector2d location, PlayerClass pc)
            : base(name, true)
        {
            Init(location, pc);
        }

        private void Init(Vector2d location, PlayerClass pc)
        {
            this.GameEntityType = EntityType.PLAYER;
            this.AddComponent(new ClassComponent(pc));

            Animation runAnim = null;
            Animation idleAnim = null;
            Animation jumpAnim = null;

            switch (pc)
            {
                case PlayerClass.OVERWEIGHT:

                    runAnim = new Animation(Image.FromFile(Directory.GetCurrentDirectory() + "/Sprites/Characters/Heavy/HeavyRedRunS.images/image 0.png"), 150);
                    runAnim.AddFrame(Image.FromFile(Directory.GetCurrentDirectory() + "/Sprites/Characters/Heavy/HeavyRedRunS.images/image 1.png"), 150);
                    runAnim.AddFrame(Image.FromFile(Directory.GetCurrentDirectory() + "/Sprites/Characters/Heavy/HeavyRedRunS.images/image 2.png"), 150);
                    runAnim.AddFrame(Image.FromFile(Directory.GetCurrentDirectory() + "/Sprites/Characters/Heavy/HeavyRedRunS.images/image 3.png"), 150);

                    idleAnim = new Animation(Image.FromFile(Directory.GetCurrentDirectory() + "/Sprites/Characters/Heavy/HeavyRedStandS.images/image 0.png"));
                    jumpAnim = new Animation(Image.FromFile(Directory.GetCurrentDirectory() + "/Sprites/Characters/Heavy/HeavyRedJumpS.images/image 0.png"));

                    break;
            }

            GraphicsComponent gcomp = new GraphicsComponent("run", runAnim);
            gcomp.AddAnimation("idle", idleAnim);
            gcomp.AddAnimation("jump", jumpAnim);
            this.AddComponent(gcomp);

            LocationComponent lcomp = new LocationComponent(location);
            this.AddComponent(lcomp);

            SizeComponent size = new SizeComponent(new Vector2d(64, 64));
            this.AddComponent(size);

            //Legacy code
            /*
            AnimationComponent acomp = new AnimationComponent();
            
            acomp.AddFrame(sprite, 2000);

            SpriteSheet sheet = new Sprites.SpriteSheet("/Sprites/test.png");
            sheet.Load();

            acomp.AddFrame(sheet.GetSprite(1, 3, 16), 2000);

            this.AddComponent(acomp);
            */

            MovementComponent mcomp = new MovementComponent(
                movement: new Vector2d(0, 0),
                maxSpeed: 10,
                acceleration: 2,
                friction: 1
                );

            this.AddComponent(mcomp);

            PlayerComponent pcomp = new PlayerComponent();
            this.AddComponent(pcomp);

            CollisionComponent ccomp = new CollisionComponent(true);
            ccomp.OnColide += (sender, other, intersectionSize) =>
            {
                //DLog.Log(sender + " is colliding with: " + other);

                //Keeping this code for documentation purposes
                //It doesn't work, you can keep pushing through slowly if you just 
                //set the vector on movement to 0.
                //mcomp.Movement = new Vector2d(0, 0);
                if (other is Wall)
                {
                    Vector2d scenter = sender.GetCenter();
                    Vector2d ocenter = other.GetCenter();

                    Vector2d pushback = null;

                    if (intersectionSize.X <= intersectionSize.Y)
                    {
                        //push left or right

                        if (scenter.X <= ocenter.X)
                        {
                            //left
                            pushback = new Vector2d(-intersectionSize.X, 0);
                        }
                        else
                        {
                            //right
                            pushback = new Vector2d(intersectionSize.X, 0);
                        }
                    }
                    else
                    {
                        //push up or down
                        if (scenter.Y <= ocenter.Y)
                        {
                            //up
                            pushback = new Vector2d(0, -intersectionSize.Y);
                        }
                        else
                        {
                            //down
                            pushback = new Vector2d(0, intersectionSize.Y);
                        }
                    }

                    //allow player to jump again!
                    if (pushback.Y < 0)
                    {
                        mcomp.Jumps = 0;
                    }

                    //lcomp.GoBack(pushback);
                    lcomp.ApplyVector(pushback, 1); //<-- TODO: get last timeFactor for diz?

                    mcomp.AddVector(pushback);
                }
            };
            this.AddComponent(ccomp);

            InputComponent icomp = new InputComponent();
            icomp.AddEventHandler(GameEvent.Type.MOUSE_MOVED, (gEvents) =>
            {
                MouseMovedGameEvent mm = (MouseMovedGameEvent)gEvents;
                pcomp.MousePosition = new Vector2d(mm.X, mm.Y);
                return false;
            });
            icomp.AddEventHandler(GameEvent.Type.KEY_PRESSED, OnKeyPressed);
            icomp.AddEventHandler(GameEvent.Type.KEY_RELEASED, OnKeyReleased);

            icomp.AppendHandler(GameEvent.Type.KEY_RELEASED, 
                (args) => 
                {
                    KeyReleasedGameEvent krEvent = (KeyReleasedGameEvent)args;

                    if(krEvent.KeyCode == (int)Keys.P)
                    {
                        Game.CaptureFPS = true;
                    }

                    return false;
                }
            );

            #region Old ass code (Collision boxes)
            /*
            //for debugging!
            Vector2d mpos = null;
            Vector2d dragSize = new Vector2d(0, 0);
            bool dragging = false;
            
            icomp.AppendHandler(GameEvent.Type.MOUSE_MOVED, (args) =>
            {
                MouseMovedGameEvent mmEvent = (MouseMovedGameEvent)args;

                if (mmEvent.Dragged)
                {
                    dragging = true;
                    if (mpos == null)
                    {
                        mpos = new Vector2d(mmEvent.X + Game.Camera.X, mmEvent.Y + Game.Camera.Y);
                    }
                    else
                    {
                        dragSize = new Vector2d(Math.Abs((mmEvent.X + Game.Camera.X)  - mpos.X), Math.Abs((mmEvent.Y + Game.Camera.Y) - mpos.Y));
                        debugDragRect = new Rectangle((int)mpos.X, (int)mpos.Y, (int)dragSize.X, (int)dragSize.Y);
                    }
                }

                return false;
            });

            icomp.AddEventHandler(GameEvent.Type.MOUSE_RELEASED, (args) =>
            {
                
                MouseReleasedGameEvent mrEvent = (MouseReleasedGameEvent)args;

                if (dragging)
                {
                    dragging = false;
                    mpos = null;
                    dragSize = new Vector2d(0, 0);

                    if (!File.Exists("collision_rectangles.txt"))
                    {
                        File.Create("collision_rectangles.txt");
                    }

                    try {
                        using (StreamWriter sw = new StreamWriter("collision_rectangles.txt", true))
                        {
                            string line = String.Format("this.AddEntity(new Wall(new Vector2d({0},{1}), new Vector2d({2},{3}))); \r\n",debugDragRect.X, debugDragRect.Y, debugDragRect.Width, debugDragRect.Height);
                            sw.WriteLine(line);
                        }
                    }
                    catch(Exception e)
                    {
                        DLog.Log("File is buisy!");

                    }

                }

                return false;
            });
             */
            #endregion 

            this.AddComponent(icomp);

            this.AddComponent(new CameraControlComponent(this));

            this.AddComponent(new GravityComponent(5.5, 7.5));
        }
 


        //ONLY FOR DEBUGGING! REMOVE THIS AND REFERENCES!
        //public Rectangle debugDragRect { get; private set; }

        public bool OnKeyPressed(GameEvent gEvent)
        {
            KeyPressedGameEvent kEvent = (KeyPressedGameEvent)gEvent;

            PlayerComponent pcomp = this.GetComponent<PlayerComponent>();

            //debugDragRect = new Rectangle(0, 0, 0, 0);

            switch ((Keys)kEvent.KeyCode)
            {
                case Keys.A:
                    pcomp.Left = true;

                    this.GetComponent<GraphicsComponent>().AnimHandler.Change("run");

                    break;
                case Keys.D:
                    pcomp.Right = true;

                    this.GetComponent<GraphicsComponent>().AnimHandler.Change("run");
                    break;
                case Keys.Space:
                    pcomp.Jump = true;
                    this.GetComponent<GraphicsComponent>().AnimHandler.Change("jump");
                    break;
            }

            return false;
        }

        public bool OnKeyReleased(GameEvent gEvent)
        {
            KeyReleasedGameEvent kEvent = (KeyReleasedGameEvent)gEvent;
            PlayerComponent pcomp = this.GetComponent<PlayerComponent>();

            switch ((Keys)kEvent.KeyCode)
            {
                case Keys.A:
                case Keys.D:
                    pcomp.Left = false;
                    pcomp.Right = false;

                    this.GetComponent<GraphicsComponent>().AnimHandler.Change("idle");

                    break;
                case Keys.Space:
                    pcomp.Jump = false;
                    this.GetComponent<GraphicsComponent>().AnimHandler.Change("idle");
                    break;
                //for debugging purposes:
                case Keys.L:
                    DLog.Log("[Player] \t Center point is: {0} ", this.GetCenter());
                    break;
            }

            return false;
        }

        public override WGObject Serialize()
        {
            GraphicsComponent gcomp = GetComponent<GraphicsComponent>();

            WGObject result = base.Serialize();

            WGArray animation = new WGArray("Animation", gcomp.AnimHandler.CurrentAnimation.ToCharArray());
            result.AddArray(animation);

            ClassComponent ccomp = this.GetComponent<ClassComponent>();
            result.AddField(new WGField("Class", (byte)ccomp.PlayerClass));

            LocationComponent lcomp = this.GetComponent<LocationComponent>();
            WGObject location = new WGObject("Location");
            location.AddField(new WGField("X", lcomp.Point.X));
            location.AddField(new WGField("Y", lcomp.Point.Y));
            result.AddObject(location);

            PlayerComponent pcomp = this.GetComponent<PlayerComponent>();
            WGObject wgAim = new WGObject("AimDirection");
            wgAim.AddField(new WGField("X", pcomp.AimDirection.X));
            wgAim.AddField(new WGField("Y", pcomp.AimDirection.Y));
            result.AddObject(wgAim);

            return result;

        }

        public static Player Deserialize(WGObject localPlayer)
        {
            WGField playerClass = localPlayer.FindField("Class");
            WGArray name = localPlayer.FindArray("Name");

            WGObject location = localPlayer.FindObject("Location");
            WGField locX = location.FindField("X");
            WGField locY = location.FindField("Y");

            int pointer = 0;

            double lx = SerializerUtil.ReadDouble(locX.Data, ref pointer);
            pointer = 0;
            double ly = SerializerUtil.ReadDouble(locY.Data, ref pointer);
            pointer = 0;
            byte pc = SerializerUtil.ReadByte(playerClass.Data, ref pointer);
            pointer = 0;
            string n = SerializerUtil.ReadString(name.Data, ref pointer, name.Count);

            return new Player(n, new Vector2d(lx, ly), (PlayerClass)pc);
        }
        /// <summary>
        /// Should never be called
        /// Each client will take care of their own update.
        /// </summary>
        /// <param name="wgoEntities"></param>
        public override void Update(WGObject wgoEntities)
        {
            
        }

        public override string GetPrefix()
        {
            return PREFIX;
        }
    }
}
