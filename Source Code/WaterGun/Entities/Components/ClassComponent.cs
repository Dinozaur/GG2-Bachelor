﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Entities.Components
{
    public class ClassComponent: IGameComponent
    {

        public ClassComponent(PlayerClass pc)
        {
            PlayerClass = pc;
        }

        public PlayerClass PlayerClass { get; private set; }
    }
}
