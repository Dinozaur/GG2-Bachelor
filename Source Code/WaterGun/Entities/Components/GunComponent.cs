﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.EventSystem;
using WaterGun.EventSystem.Types;
using WaterGun.Maths;

namespace WaterGun.Entities.Components
{
    public class GunComponent : IGameComponent
    {
        public GunComponent(Image ammo)
        {
            Cooldown = 0;
            AimDirection = new Vector2d(1, 0);
            MousePosition = new Vector2d(0, 0);
            AmmoSprite = ammo;
            IsShooting = false;
            IdleOffset = new Vector2d(10, 25);
            FiringOffset = new Vector2d(0, 15);
            RenderOffset = IdleOffset;
        }

        public double Cooldown { get; set; }
        public Vector2d AimDirection { get; set; }
        public Vector2d MousePosition { get; set; }
        public Image AmmoSprite { get; private set; }
        public bool IsShooting { get; set; }
        public Vector2d RenderOffset { get; set; }
        public Vector2d FiringOffset { get; private set; }
        public Vector2d IdleOffset { get; private set; }

        public void Update(int deltaTime)
        {
            Cooldown = Cooldown < 0 ? 0 : Cooldown - deltaTime;
        }
    }
}
