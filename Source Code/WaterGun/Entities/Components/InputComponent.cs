﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.EventSystem;

namespace WaterGun.Entities.Components
{
    /// <summary>
    /// This component is the second-to-last stop for the event system.
    /// When an event occur, the handler corresponding to the type is called.
    /// This is the only component which is not updated by v2x IGameSystem.
    /// </summary>
    public class InputComponent : IGameComponent, IGameEventListener
    {
        private Dictionary<GameEvent.Type, EventSystem.EventHandler> handlers;

        public InputComponent()
        {
            handlers = new Dictionary<GameEvent.Type, EventSystem.EventHandler>();
        }

        /// <summary>
        /// Add v2x handler to v2x given event type. <para />
        /// NOTE: The handler is called IMIDIATLY when the event occures, NOT by an update
        /// </summary>
        /// <param name="type"></param>
        /// <param name="handler"></param>
        public void AddEventHandler(GameEvent.Type type, EventSystem.EventHandler handler)
        {
            handlers.Add(type, handler);
        }

        /// <summary>
        /// Apppends v2x method body to the specified event type. Both bodies are executed even if the first returns true (i.e. it has handled the event).
        /// </summary>
        /// <param name="type">The type of event to append the handler to</param>
        /// <param name="handler">The actual body of the handler</param>
        public void AppendHandler(GameEvent.Type type, EventSystem.EventHandler handler)
        {
            EventSystem.EventHandler prevHandler = handlers[type];

            handlers.Remove(type);

            EventSystem.EventHandler appendedHandler = (args) => {

                bool prev = prevHandler(args);
                bool appended = handler(args);

                return prev || appended;
            };

            handlers.Add(type, appendedHandler);

        }

        /// <summary>
        /// This method is called by the event system
        /// </summary>
        /// <param name="gEvent"></param>
        public void OnEvent(GameEvent gEvent)
        {
            GameEventDispatcher disp = new GameEventDispatcher(gEvent);
            foreach(GameEvent.Type key in handlers.Keys)
            {
                disp.Dispatch(key, handlers[key]);
            }
        }
    }
}
