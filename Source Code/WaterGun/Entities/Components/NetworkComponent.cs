﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Entities.Components
{
    public class NetworkComponent : IGameComponent
    {
        public NetworkComponent()
        {
            HasBeenUpdated = false;
        }

        public bool HasBeenUpdated { get; set; }
    }
}
