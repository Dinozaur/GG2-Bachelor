﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Maths;

namespace WaterGun.Entities.Components
{
    class MovementComponent: IGameComponent
    {

        public MovementComponent(Vector2d movement, double maxSpeed, double acceleration, double friction)
        {
            Movement = movement;
            MaxSpeed = maxSpeed;
            Acceleration = acceleration;
            Friction = friction;

            //TODO: set these in parameter list
            MaxJumps = 1; //<-- should be class specific
        }

        public Vector2d Movement { get; set; }
        public double MaxSpeed { get; set; }
        public double Acceleration { get; set; }
        public double Friction { get; set; }
        public int MaxJumps { get; set; } //<-- private set?
        public int Jumps { get; set; }

        /// <summary>
        /// Should only be called from v2x system (MovementSystem to be exact)
        /// </summary>
        /// <param name="jumpHeight"></param>
        public void Jump(double jumpHeight)
        {
            if (Jumps < MaxJumps)
            {
                Jumps++;
                Vector2d force = new Vector2d(0, -jumpHeight);
                AddVector(force);
            }
        }

        public void SubtractVector(Vector2d vector)
        {
            if(Movement.CalcMagnitude() <= Friction)
            {
                Movement = new Vector2d(0, 0);
            }
            else
            {
                Movement -= vector;
            }
        }

        public void AddVector(Vector2d vector)
        {
            Movement += vector;

            double magnitude = Movement.CalcMagnitude();

            if (Math.Abs(Movement.X) > MaxSpeed)
            {
                double direction = Movement.InRadians();

                //double dx = magnitude * Math.Cos(direction);
                double dx = MaxSpeed * (Movement.X < 0 ? -1 : 1);
                double dy = magnitude * Math.Sin(direction);

                Movement = new Vector2d(dx, dy);                
            }
        }
    }
}
