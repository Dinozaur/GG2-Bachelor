﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Maths;

namespace WaterGun.Entities.Components
{
    /// <summary>
    /// Used to handle input handling for the player, and NOT the movement.
    /// </summary>
    public class PlayerComponent: IGameComponent
    {
        public PlayerComponent()
        {
            Right = false;
            Left = false;
            Jump = false;
            AimDirection = new Vector2d(1, 0);
            MousePosition = new Vector2d(0, 0);
            Health = 100;
        }
        
        public bool Right { get; set; }
        public bool Left { get; set; }
        public bool Jump { get; set; }
        public Vector2d AimDirection { get; set; }
        public Vector2d MousePosition { get; set; }
        public int Health { get; private set; }

        public void TakeDamage(int damage)
        {
            Health -= damage;
        }

        public bool IsDead()
        {
            return Health <= 0;
        }

        public void ResetHealth()
        {
            Health = 100;
        }
    }
}
