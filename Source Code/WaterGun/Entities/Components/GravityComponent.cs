﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Maths;

namespace WaterGun.Entities.Components
{
    public class GravityComponent : IGameComponent
    {
        public GravityComponent(double pull, double maxPull)
        {
            Pull = new Vector2d(0, pull);
            MaxPull = maxPull;
        }

        public Vector2d Pull { get; private set; }
        public double MaxPull;

        public void Update(double factor)
        {
            Pull *= factor;

            if (Pull.Y > MaxPull)
            {
                Pull = new Vector2d(0, MaxPull);
            }
        }
    }
}
