﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Maths;

namespace WaterGun.Entities.Components
{
    public class GraphicsComponent : IGameComponent
    {
        public GraphicsComponent(string name, Animation anim)
        {
            AnimHandler = new AnimationHandler(name, anim);
        }

        public AnimationHandler AnimHandler { get; private set; }

        public void Update(int deltaTime)
        {
            AnimHandler.Update(deltaTime);
        }

        public void AddAnimation(string name, Animation anim)
        {
            AnimHandler.AddAnimation(name, anim);
        }

        public Image GetImage()
        {
            return AnimHandler.GetImage();
        }

        public void RotateImage(Vector2d direction, bool flip)
        {
            AnimHandler.Rotate(direction, flip);
        }
    }
}

