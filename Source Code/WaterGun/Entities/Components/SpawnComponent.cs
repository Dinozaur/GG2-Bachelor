﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Entities.Components
{
    public class SpawnComponent : IGameComponent
    {
        public enum TeamColor
        {
            BLUE,
            RED
        }

        public SpawnComponent(TeamColor team)
        {
            Team = team;
        }

        public TeamColor Team { get; private set; }
    }
}
