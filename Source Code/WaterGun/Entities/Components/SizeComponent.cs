﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Maths;

namespace WaterGun.Entities.Components
{
    public class SizeComponent : IGameComponent
    {
        public SizeComponent(Vector2d size)
        {
            Size = size;
        }

        public Vector2d Size { get; private set; }
    }
}
