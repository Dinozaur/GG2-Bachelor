﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Entities.Components
{
    public class CameraControlComponent : IGameComponent
    {
        public CameraControlComponent(Entity controllingEntity)
        {
            ControllingEntity = controllingEntity;
        }

        public Entity ControllingEntity { get; set; }
    }
}
