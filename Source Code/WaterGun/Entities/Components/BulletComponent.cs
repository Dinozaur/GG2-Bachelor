﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Maths;

namespace WaterGun.Entities.Components
{
    public class BulletComponent : IGameComponent
    {
        public delegate void BulletEvent(Entity sender);

        public BulletComponent(double maxDistance)
        {
            TotalDistance = maxDistance;
            DistanceLeft = maxDistance;
            OnMaxDistanceReached += (e) => { };
        }

        public double TotalDistance { get; set; }

        public double DistanceLeft { get; set; }

        public void Update(Entity sender, double distance)
        {
            DistanceLeft -= distance;

            if (DistanceLeft <= 0)
            {
                OnMaxDistanceReached(sender);
            }
        }

        public event BulletEvent OnMaxDistanceReached;
    }
}
