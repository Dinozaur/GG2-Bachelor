﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Maths;

namespace WaterGun.Entities.Components
{
    class CollisionComponent: IGameComponent
    {
        public delegate void CollisionEvent(Entity sender, Entity other, Vector2d intersectionSize);

        public bool Solid { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }

        public CollisionComponent(bool solid, int x = 0, int y = 0)
        {
            Solid = solid;
            X = x;
            Y = y;

            OnColide += (s, o, i) => { };
        }

        public void Collide(Entity sender, Entity other, Vector2d intersectionSize)
        {
            OnColide(sender, other, intersectionSize);
        }

        public event CollisionEvent OnColide;
    }
}
