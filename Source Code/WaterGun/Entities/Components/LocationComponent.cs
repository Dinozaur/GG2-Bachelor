﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Maths;

namespace WaterGun.Entities.Components
{
    public class LocationComponent : IGameComponent
    {
        public LocationComponent(Vector2d point)
        {
            Point = point;
        }

        public Vector2d Point { get; private set; }

        public void SetPoint(Vector2d point)
        {
            Point = point;
        }

        public void ApplyVector(Vector2d vector, double timeFactor)
        {
            Point += (vector * timeFactor);
        }

        public void GoBack(Vector2d movement)
        {
            Point = new Vector2d(Point.X - movement.X, Point.Y - movement.Y); 
        }
    }
}
