﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Maths;

namespace WaterGun
{
    public class Camera
    {
        public Camera(int x, int y, int width, int height, int maxX, int maxY)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            MaxX = maxX;
            MaxY = maxY;
        }

        public int X { get; set; } //TODO: set back to private set
        public int Y { get; set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        //NOTE: MaxX + Width should not be greater than world width in pixels
        public int MaxX { get; set; }
        public int MaxY { get; set; }

        /// <summary>
        /// Moves the center of the camera to the given position (but keeps the camera in bounds)
        /// </summary>
        /// <param name="x">The desired X position of the center</param>
        /// <param name="y">The desired Y position of the center</param>
        public void SetCenter(int x, int y)
        {
            X = x - Width / 2;
            Y = y - Height / 2;
            KeepInBounds();
        }

        public void SetCenter(Vector2d center)
        {
            SetCenter((int)center.X, (int)center.Y);
        }

        /// <summary>
        /// Makes sure the camera does not film beyond bounds. Should be called after every camera-moving statement.
        /// </summary>
        private void KeepInBounds()
        {
            X = X < 0 ? 0 : X;
            Y = Y < 0 ? 0 : Y;

            X = X > MaxX ? MaxX : X;
            Y = Y > MaxY ? MaxY : Y;
        }

    }
}
