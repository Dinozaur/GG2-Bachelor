﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;

namespace WaterGun.Net
{
    public class WGClient
    {
        private static int RECIEVE_BUFFER_SIZE = 8000;

        private TcpClient sender;
        private TcpClient reciever;

        public WGClient(string ip, int port)
        {
            IP = ip;
            Port = port;

            sender = new TcpClient();
            reciever = new TcpClient();

            sender.ReceiveBufferSize = RECIEVE_BUFFER_SIZE;
            reciever.ReceiveBufferSize = RECIEVE_BUFFER_SIZE;
            sender.SendBufferSize = RECIEVE_BUFFER_SIZE;
            reciever.SendBufferSize = RECIEVE_BUFFER_SIZE;

            OnDataRecieved += (b) => { };
        }

        public string IP { get; set; }
        public int Port { get; set; }

        public void Connect()
        {
            if (!sender.Connected)
            {
                sender.Connect(IP, Port);
            }
            if (!reciever.Connected)
            {
                reciever.Connect(IP, Port);
            }

            new Thread(() => {

                byte[] bytes = new byte[RECIEVE_BUFFER_SIZE];

                int readPointer = 0;
                while (reciever.Connected)
                {
                    if (reciever.Available > 0)
                    {
                        NetworkStream netStream = reciever.GetStream();
                        int readCount = netStream.Read(bytes, readPointer, bytes.Length - readPointer);
                        readPointer += readCount;
                        //reciever.Client.Receive(bytes);

                        if (readPointer == RECIEVE_BUFFER_SIZE)
                        {
                            OnDataRecieved(bytes);
                            Array.Clear(bytes, 0, bytes.Length);
                            readPointer = 0;
                        }
                    }
                }
            
            }).Start();
        }

        public void Send(byte[] data)
        {
            byte[] buffer = new byte[RECIEVE_BUFFER_SIZE];
            Array.Copy(data, buffer, data.Length);
            NetworkStream netStream = sender.GetStream();
            netStream.Write(buffer, 0, buffer.Length);
        }

        public void Disconnect()
        {
            if (sender.Connected)
            {
                sender.Client.Disconnect(false);
            }
            if (reciever.Connected)
            {
                reciever.Client.Disconnect(false);
            }
        }

        public delegate void ClientEvent(byte[] data);
        public event ClientEvent OnDataRecieved;
    }
}
