﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Scenes
{
    public class Tile
    {
        public Tile(Image img, int tileX, int tileY, int size)
        {
            Image = img;
            TileX = tileX;
            TileY = tileY;
            Size = size;
        }

        public Image Image { get; private set; }
        public int TileX{ get; private set; }
        public int TileY { get; private set; }
        public int Size{ get; private set; }
    }
}
