﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WaterGun.Entities;
using WaterGun.Entities.Components;
using WaterGun.Entities.Mobs;
using WaterGun.EventSystem.Types;
using WaterGun.Maths;
using WaterGun.Sounds;

namespace WaterGun.Scenes.Levels
{
    public class KothHarvest : Level
    {
        private static readonly string BG_FILE = "/Levels/Data/koth_harvest.png";
        private static readonly int BG_SCALE = 6;

        public KothHarvest()
            : base("koth_harvest")
        {
            //TODO: move image load to init method?
            LoadBackground();
            PlayIngameMusic();
        }

        public override Image GetScreenshot(Camera camera)
        {
            //render background:
            Image img = new Bitmap(camera.Width, camera.Height);

            using (Graphics g = Graphics.FromImage(img))
            {
                g.DrawImage(Background, 0,0, new Rectangle(camera.X, camera.Y, camera.Width, camera.Height), GraphicsUnit.Pixel);

                foreach(Entity ent in this.GetEntities()) {
                    if (ent is Wall)
                    {
                        LocationComponent lcomp = ent.GetComponent<LocationComponent>();
                        SizeComponent scomp = ent.GetComponent<SizeComponent>();

                        g.FillRectangle(new SolidBrush(Color.FromArgb(100, 0xcc, 0, 0xcc)), new Rectangle((int)lcomp.Point.X - camera.X, (int)lcomp.Point.Y - camera.Y, (int)scomp.Size.X, (int)scomp.Size.Y));
                    }

                    /*
                    if (ent is Player)
                    {
                        Rectangle debugRect = ((Player)ent).debugDragRect;

                        Rectangle screenRect = new Rectangle(debugRect.X - camera.X, debugRect.Y - camera.Y, debugRect.Width, debugRect.Height);

                        g.FillRectangle(new SolidBrush(Color.FromArgb(100, 0x00, 0xcc, 0x00)), screenRect);
                    }
                     */

                    if (ent.HasComponent<GraphicsComponent>())
                    {
                        GraphicsComponent gcomp = ent.GetComponent<GraphicsComponent>();
                        LocationComponent lcomp = ent.GetComponent<LocationComponent>();

                        //TODO: find better place/solution for this "hack"
                        Vector2d offset = ent.HasComponent<GunComponent>() ? ent.GetComponent<GunComponent>().RenderOffset : new Vector2d(0, 0);

                        Image entImg = gcomp.GetImage();

                        g.DrawImage(gcomp.GetImage(), new Point((int)((lcomp.Point.X + offset.X) - camera.X), (int)((lcomp.Point.Y + offset.Y) - camera.Y)));
                    }

                }

            }

            return img;
        }

        public override void Init(Camera camera)
        {
            //throw new NotImplementedException();

            camera.MaxX = 800 * BG_SCALE - camera.Width;
            camera.MaxY = 200 * BG_SCALE - camera.Height;

            //Floor collision
            #region
            Wall floor = new Wall(new Vector2d(0, 165 * BG_SCALE), new Vector2d(800 * BG_SCALE + 36, 30 * BG_SCALE));
            this.AddEntity(floor);
            Wall floor1 = new Wall(new Vector2d(0, 795), new Vector2d(1245, 60));
            this.AddEntity(floor1);
            Wall floor2 = new Wall(new Vector2d(1245, 825), new Vector2d(400, 60));
            this.AddEntity(floor2);
            this.AddEntity(new Wall(new Vector2d(1645, 795), new Vector2d(400, 60)));
            this.AddEntity(new Wall(new Vector2d(2045, 825), new Vector2d(90, 60)));
            this.AddEntity(new Wall(new Vector2d(2222, 825), new Vector2d(410, 15)));
            this.AddEntity(new Wall(new Vector2d(2732, 825), new Vector2d(100, 60)));
            this.AddEntity(new Wall(new Vector2d(2832, 795), new Vector2d(400, 60)));
            this.AddEntity(new Wall(new Vector2d(3232, 825), new Vector2d(390, 60)));
            this.AddEntity(new Wall(new Vector2d(3622, 795), new Vector2d(1200, 60)));
            #endregion

            //Pit Collision
            #region
            this.AddEntity(new Wall(new Vector2d(2726, 833), new Vector2d(9, 24)));

            this.AddEntity(new Wall(new Vector2d(2719, 845), new Vector2d(10, 23)));

            this.AddEntity(new Wall(new Vector2d(2714, 848), new Vector2d(8, 45)));

            this.AddEntity(new Wall(new Vector2d(2709, 855), new Vector2d(9, 30)));

            this.AddEntity(new Wall(new Vector2d(2704, 859), new Vector2d(7, 28)));

            this.AddEntity(new Wall(new Vector2d(2685, 867), new Vector2d(21, 24)));

            this.AddEntity(new Wall(new Vector2d(2680, 873), new Vector2d(8, 18)));

            this.AddEntity(new Wall(new Vector2d(2673, 880), new Vector2d(11, 18)));

            this.AddEntity(new Wall(new Vector2d(2667, 884), new Vector2d(8, 21)));

            this.AddEntity(new Wall(new Vector2d(2662, 891), new Vector2d(16, 23)));

            this.AddEntity(new Wall(new Vector2d(2654, 900), new Vector2d(14, 19)));

            this.AddEntity(new Wall(new Vector2d(2647, 904), new Vector2d(21, 18)));

            this.AddEntity(new Wall(new Vector2d(2646, 910), new Vector2d(15, 18)));

            this.AddEntity(new Wall(new Vector2d(2639, 914), new Vector2d(20, 16)));

            this.AddEntity(new Wall(new Vector2d(2633, 921), new Vector2d(22, 18)));

            this.AddEntity(new Wall(new Vector2d(2629, 927), new Vector2d(16, 20)));

            this.AddEntity(new Wall(new Vector2d(2622, 931), new Vector2d(19, 23)));

            this.AddEntity(new Wall(new Vector2d(2608, 937), new Vector2d(20, 17)));

            this.AddEntity(new Wall(new Vector2d(2601, 943), new Vector2d(18, 16)));

            this.AddEntity(new Wall(new Vector2d(2594, 950), new Vector2d(23, 12)));

            this.AddEntity(new Wall(new Vector2d(2590, 957), new Vector2d(36, 9)));

            this.AddEntity(new Wall(new Vector2d(2584, 963), new Vector2d(20, 13)));

            this.AddEntity(new Wall(new Vector2d(2570, 968), new Vector2d(22, 18)));

            this.AddEntity(new Wall(new Vector2d(2568, 976), new Vector2d(18, 14)));

            this.AddEntity(new Wall(new Vector2d(2535, 980), new Vector2d(55, 19)));

            this.AddEntity(new Wall(new Vector2d(2522, 985), new Vector2d(29, 9)));

            this.AddEntity(new Wall(new Vector2d(2340, 986), new Vector2d(2, 3)));

            this.AddEntity(new Wall(new Vector2d(2321, 985), new Vector2d(23, 8)));

            this.AddEntity(new Wall(new Vector2d(2309, 980), new Vector2d(17, 6)));

            this.AddEntity(new Wall(new Vector2d(2284, 971), new Vector2d(20, 11)));

            this.AddEntity(new Wall(new Vector2d(2275, 963), new Vector2d(10, 8)));

            this.AddEntity(new Wall(new Vector2d(2270, 956), new Vector2d(8, 8)));

            this.AddEntity(new Wall(new Vector2d(2267, 952), new Vector2d(6, 7)));

            this.AddEntity(new Wall(new Vector2d(2257, 946), new Vector2d(10, 10)));

            this.AddEntity(new Wall(new Vector2d(2248, 937), new Vector2d(12, 11)));

            this.AddEntity(new Wall(new Vector2d(2232, 934), new Vector2d(11, 7)));

            this.AddEntity(new Wall(new Vector2d(2228, 926), new Vector2d(10, 8)));

            this.AddEntity(new Wall(new Vector2d(2219, 920), new Vector2d(8, 10)));

            this.AddEntity(new Wall(new Vector2d(2218, 912), new Vector2d(5, 12)));

            this.AddEntity(new Wall(new Vector2d(2212, 909), new Vector2d(5, 11)));

            this.AddEntity(new Wall(new Vector2d(2208, 904), new Vector2d(4, 10)));

            this.AddEntity(new Wall(new Vector2d(2197, 898), new Vector2d(8, 15)));

            this.AddEntity(new Wall(new Vector2d(2191, 890), new Vector2d(7, 22)));

            this.AddEntity(new Wall(new Vector2d(2189, 885), new Vector2d(3, 22)));

            this.AddEntity(new Wall(new Vector2d(2178, 880), new Vector2d(4, 20)));

            this.AddEntity(new Wall(new Vector2d(2173, 878), new Vector2d(15, 24)));

            this.AddEntity(new Wall(new Vector2d(2177, 872), new Vector2d(7, 35)));

            this.AddEntity(new Wall(new Vector2d(2163, 867), new Vector2d(13, 46)));

            this.AddEntity(new Wall(new Vector2d(2155, 864), new Vector2d(9, 36)));

            this.AddEntity(new Wall(new Vector2d(2158, 860), new Vector2d(4, 13)));

            this.AddEntity(new Wall(new Vector2d(2149, 856), new Vector2d(12, 24)));

            this.AddEntity(new Wall(new Vector2d(2141, 849), new Vector2d(11, 19)));

            this.AddEntity(new Wall(new Vector2d(2133, 842), new Vector2d(13, 26)));

            this.AddEntity(new Wall(new Vector2d(2129, 833), new Vector2d(13, 18)));
            #endregion

            //Roof Collision
            #region
            this.AddEntity(new Wall(new Vector2d(665, 675), new Vector2d(95, 15)));

            this.AddEntity(new Wall(new Vector2d(2039, 644), new Vector2d(8, 0)));

            this.AddEntity(new Wall(new Vector2d(1703, 644), new Vector2d(340, 7)));

            this.AddEntity(new Wall(new Vector2d(2087, 633), new Vector2d(160, 10)));

            this.AddEntity(new Wall(new Vector2d(2362, 603), new Vector2d(135, 6)));

            this.AddEntity(new Wall(new Vector2d(2620, 634), new Vector2d(160, 11)));

            this.AddEntity(new Wall(new Vector2d(2816, 645), new Vector2d(340, 8)));

            this.AddEntity(new Wall(new Vector2d(3960, 651), new Vector2d(67, 12)));

            this.AddEntity(new Wall(new Vector2d(4110, 674), new Vector2d(79, 13)));

            this.AddEntity(new Wall(new Vector2d(4310, 650), new Vector2d(133, 7)));

            this.AddEntity(new Wall(new Vector2d(833, 649), new Vector2d(68, 16)));

            this.AddEntity(new Wall(new Vector2d(426, 649), new Vector2d(135, 10)));

            this.AddEntity(new Wall(new Vector2d(561, 657), new Vector2d(5, 6)));

            this.AddEntity(new Wall(new Vector2d(567, 663), new Vector2d(5, 7)));

            this.AddEntity(new Wall(new Vector2d(576, 670), new Vector2d(8, 4)));

            this.AddEntity(new Wall(new Vector2d(580, 678), new Vector2d(8, 8)));

            this.AddEntity(new Wall(new Vector2d(588, 681), new Vector2d(9, 11)));

            this.AddEntity(new Wall(new Vector2d(598, 695), new Vector2d(6, 7)));

            this.AddEntity(new Wall(new Vector2d(607, 702), new Vector2d(16, 7)));

            this.AddEntity(new Wall(new Vector2d(1004, 674), new Vector2d(18, 6)));

            this.AddEntity(new Wall(new Vector2d(1017, 669), new Vector2d(19, 6)));

            this.AddEntity(new Wall(new Vector2d(1027, 665), new Vector2d(17, 6)));

            this.AddEntity(new Wall(new Vector2d(1042, 659), new Vector2d(17, 7)));

            this.AddEntity(new Wall(new Vector2d(1056, 650), new Vector2d(15, 9)));

            this.AddEntity(new Wall(new Vector2d(1067, 646), new Vector2d(42, 4)));

            this.AddEntity(new Wall(new Vector2d(1075, 639), new Vector2d(18, 7)));

            this.AddEntity(new Wall(new Vector2d(1102, 651), new Vector2d(18, 8)));

            this.AddEntity(new Wall(new Vector2d(1115, 657), new Vector2d(17, 7)));

            this.AddEntity(new Wall(new Vector2d(1128, 662), new Vector2d(15, 8)));

            this.AddEntity(new Wall(new Vector2d(1137, 669), new Vector2d(18, 6)));

            this.AddEntity(new Wall(new Vector2d(1141, 672), new Vector2d(24, 7)));

            this.AddEntity(new Wall(new Vector2d(1692, 649), new Vector2d(13, 9)));

            this.AddEntity(new Wall(new Vector2d(1691, 657), new Vector2d(13, 6)));

            this.AddEntity(new Wall(new Vector2d(1684, 665), new Vector2d(27, 5)));

            this.AddEntity(new Wall(new Vector2d(1675, 669), new Vector2d(32, 5)));

            this.AddEntity(new Wall(new Vector2d(1673, 674), new Vector2d(13, 8)));

            this.AddEntity(new Wall(new Vector2d(1665, 683), new Vector2d(18, 4)));

            this.AddEntity(new Wall(new Vector2d(1658, 687), new Vector2d(19, 6)));
            #endregion


            SpawnEntity blue1 = new SpawnEntity(new Vector2d(2040, 700), SpawnComponent.TeamColor.BLUE);
            this.blueSpawns.Add(blue1);

            SpawnEntity red1 = new SpawnEntity(new Vector2d(2040, 700), SpawnComponent.TeamColor.RED);
            this.redSpawn.Add(red1);

           // SpawnEntity red2 = new SpawnEntity(new Vector2d(3300, 360), SpawnComponent.TeamColor.RED);
            //this.redSpawn.Add(red2);
        }

        public override void LoadBackground()
        {
            Image img = Image.FromFile(Directory.GetCurrentDirectory() + BG_FILE);

            Background = new Bitmap(img.Width * BG_SCALE, img.Height * BG_SCALE);

            using (Graphics g = Graphics.FromImage(Background))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                g.DrawImage(img, 0, 0, Background.Width, Background.Height);
            }
        }


        private static void PlayIngameMusic()
        {
            SoundManager manager = SoundManager.GetInstance();
            Sound ingameMusic = new Sound("Game", "Sounds/Music/ingamemusic.wav", true);

            manager.AddSound(ingameMusic);
            manager.PlaySound("Game", SoundManager.SoundLevel.Level);
        }
    }
}
