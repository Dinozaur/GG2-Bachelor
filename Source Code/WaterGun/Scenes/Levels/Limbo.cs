﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities;
using WaterGun.Entities.Components;
using WaterGun.Entities.Mobs;
using WaterGun.Maths;
using WaterGun.Sprites;

namespace WaterGun.Scenes.Levels
{
    public class Limbo : TestLevel
    {
        private readonly string layout =
            "W------------------------W" +
            "W------------------------W" +
            "W------------------------W" +
            "W------------------------W" +
            "W------------------------W" +
            "W------------------------W" +
            "W------------------------W" +
            "W------------------------W" +
            "W------------------------W" +
            "W------------------------W" +
            "W-------WWWWWWWWWW-------W" +
            "W------------------------W" +
            "W------------------------W" +
            "WWWWWWWWWWWWWWWWWWWWWWWWWW";


        private SpriteSheet tileSheet;
        

        public Limbo() : base("kothHarvest", 16)
        {
            tiles = new List<Tile>();
            tileSheet = new SpriteSheet("/Sprites/test.png");
            TilesWide = 26;
            TilesHigh = 14;
        }

        public int TilesWide { get; private set; }
        public int TilesHigh { get; private set; }

        public override void Init(Camera cam)
        {
            //tiles
            tileSheet.Load();

            Image wall = tileSheet.GetSprite(0,2,16);
            Image air = tileSheet.GetSprite(3,2,16);

            for (int y = 0; y < TilesHigh; y++)
            {
                for (int x = 0; x < TilesWide; x++)
                {
                    int layoutPos = x + y * TilesWide;
                    switch (layout[layoutPos])
                    {
                        case 'W':
                            tiles.Add(new Tile(wall, x, y, TileSize));
                            break;
                        case '-':
                            tiles.Add(new Tile(air, x, y, TileSize));
                            break;
                    }
                }
            }

            //static entities
            Wall floor = new Wall(new Vector2d(0, 13 * 16), new Vector2d(26 * 16, 16));
            this.AddEntity(floor);

            Wall leftWall = new Wall(new Vector2d(0, 0), new Vector2d(16, 26 * 16));
            this.AddEntity(leftWall);

            Wall rightWall = new Wall(new Vector2d((TilesWide - 1) * TileSize, 0), new Vector2d(16, TilesHigh * TileSize));
            this.AddEntity(rightWall);

            //TODO: make as platform
            Wall platform = new Wall(new Vector2d(8 * TileSize, 10 * TileSize), new Vector2d(10 * TileSize, TileSize));
            this.AddEntity(platform);

            //other entities
            Image playerSprite = tileSheet.GetSprite(1, 3, 16);
            Vector2d location = new Vector2d(100, 100);
            Player player = new Player(0, location, PlayerClass.OVERWEIGHT);
            this.AddEntity(player);

            Image gunSprite = tileSheet.GetSprite(2, 3, 16);
            Gun gun = new Gun(player.GetComponent<LocationComponent>(), PlayerClass.OVERWEIGHT);
            this.AddEntity(gun);
        }

        public override Image GetScreenshot(Camera camera)
        {
            Image screenshot = new Bitmap(camera.Width, camera.Height);
            //viewport is slightly bigger than the camera (if camera is partly in v2x tile)
            Rectangle viewport = new Rectangle(
                camera.X - camera.X % TileSize,
                camera.Y - camera.Y % TileSize,
                camera.Width + (TileSize - camera.Width % TileSize),
                camera.Height + (TileSize - camera.Height % TileSize)
                );

            using (Image viewportImg = new Bitmap(viewport.Width, viewport.Height))
            {
                using (Graphics g = Graphics.FromImage(viewportImg))
                {
                    //TODO: use da math to get indexes
                    for (int i = 0; i < tiles.Count; i++)
                    {
                        Tile tile = tiles[i];

                        int pixelPosX = tile.TileX * TileSize;
                        int pixelPosY = tile.TileY * TileSize;

                        if (viewport.Contains(pixelPosX, pixelPosY))
                        {
                            g.DrawImage(tile.Image, pixelPosX - viewport.X, pixelPosY - viewport.Y);
                        }
                    }

                    //Test entities
                    foreach (Entity ent in this.GetEntities())
                    {
                        /*
                        if (ent is Wall)
                        {
                            if (ent.HasComponent<LocationComponent>() && ent.HasComponent<SizeComponent>())
                            {
                                LocationComponent lcomp = ent.GetComponent<LocationComponent>();
                                SizeComponent scomp = ent.GetComponent<SizeComponent>();
                                if (viewport.Contains((int)lcomp.Point.X, (int)lcomp.Point.Y))
                                {
                                    g.FillRectangle(new SolidBrush(Color.Purple), new Rectangle((int)(lcomp.Point.X - viewport.X), (int)(lcomp.Point.Y - viewport.Y), (int)scomp.Size.X, (int)scomp.Size.Y));
                                }
                            }
                        }
                         */

                        if (ent.HasComponent<GraphicsComponent>() && 
                            ent.HasComponent<LocationComponent>())
                        {
                            GraphicsComponent gcomp = ent.GetComponent<GraphicsComponent>();
                            LocationComponent lcomp = ent.GetComponent<LocationComponent>();

                            g.DrawImage(gcomp.GetImage(), (int)(lcomp.Point.X - viewport.X), (int)(lcomp.Point.Y - viewport.Y));
                        }
                    }
                }

                using (Graphics g = Graphics.FromImage(screenshot))
                {
                    Rectangle screenshotViewportBounds = new Rectangle(
                        viewport.X - camera.X, 
                        viewport.Y - camera.Y, 
                        camera.Width, 
                        camera.Height
                        );

                    g.DrawImage(viewportImg, 0, 0, screenshotViewportBounds, GraphicsUnit.Pixel);
                }
            }

            return screenshot;
        }
    }
}
