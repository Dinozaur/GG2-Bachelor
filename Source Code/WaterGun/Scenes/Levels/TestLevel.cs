﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities.Mobs;

namespace WaterGun.Scenes.Levels
{
    public abstract class TestLevel : Scene
    {
        protected List<Tile> tiles; 

        public TestLevel(string name, int tileWidth)
            : base(name)
        {
            tiles = new List<Tile>();
            TileSize = tileWidth;
        }

        public int TileSize { get; private set; }

        //public abstract Player GetLocalPlayer();
    }
}
