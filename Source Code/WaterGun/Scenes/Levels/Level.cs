﻿using GameSerializer;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WaterGun.Entities;
using WaterGun.Entities.Components;
using WaterGun.Entities.Mobs;
using WaterGun.EventSystem;
using WaterGun.EventSystem.Types;
using WaterGun.Maths;

namespace WaterGun.Scenes.Levels
{
    public abstract class Level : Scene
    {
        protected List<SpawnEntity> blueSpawns;
        protected List<SpawnEntity> redSpawn;

        protected Dictionary<string, Entity> namedEntities;

        protected Player player;

        public Level(string name) : base(name)
        {
            blueSpawns = new List<SpawnEntity>();
            redSpawn = new List<SpawnEntity>();
            namedEntities = new Dictionary<string, Entity>();
        }

        public Image Background { get; protected set; }

        public abstract void LoadBackground();

        /// <summary>
        /// Selects v2x random spawn from the given team, and sets the player location to be that point.          <para />
        /// NOTE: The player is NOT added to any of the entity lists for this scene/level in this method!
        /// </summary>
        /// <param name="player"></param>
        /// <param name="team"></param>
        public void SpawnPlayer(Player player, SpawnComponent.TeamColor team)
        {
            this.player = player;

            List<SpawnEntity> spawns;
            switch (team)
            {
                case SpawnComponent.TeamColor.RED:
                    spawns = redSpawn;
                    break;
                default: //<-- to make sure that spawns variable is initialized (we don't actually know that there are only two TeamColor's
                    spawns = blueSpawns;
                    break;
            }

            int rnd = Game.Dice.Next(spawns.Count);

            LocationComponent plcomp = player.GetComponent<LocationComponent>();
            LocationComponent slcomp = spawns[rnd].GetComponent<LocationComponent>();

            plcomp.SetPoint(slcomp.Point.Copy());
        }

        public Player GetLocalPlayer()
        {
            return player;
        }

        public void UpdateNetEntities(WGObject entityContainer)
        {
            List<WGObject> entities = entityContainer.Objects;

            List<string> updatedEntityNames = new List<string>();
            WGField wgClientId = entityContainer.FindField("ClientID");
            int pointer = 0;
            uint clientID = (uint)SerializerUtil.ReadLong(wgClientId.Data, ref pointer);

            foreach (WGObject wgEntity in entities)
            {
                WGArray wgName = wgEntity.FindArray("Name");

                pointer = 0;
                string name = SerializerUtil.ReadString(wgName.Data, ref pointer, wgName.Count);
                updatedEntityNames.Add(name);

                if (namedEntities.ContainsKey(name))
                {
                    Entity ent = namedEntities[name];
                    ent.Update(wgEntity);
                    ent.GetComponent<NetworkComponent>().HasBeenUpdated = true;
                }
                else
                {
                    //TODO: spawn new entity
                    WGField wgType = wgEntity.FindField("EntityType");

                    pointer = 0;
                    byte type = SerializerUtil.ReadByte(wgType.Data, ref pointer);
                    Entity ent = null;

                    switch ((Entity.EntityType)type)
                    {
                        case Entity.EntityType.BULLET:
                            ent = Bullet.Deserialize(wgEntity);
                            ent.GetComponent<CollisionComponent>().OnColide += 
                                (sender, other, intersectionSize) => 
                                {
                                    Bullet.OnCollide(this, sender, other, intersectionSize);
                                };
                            break;
                        case Entity.EntityType.GUN:
                            ent = Gun.Deserialize(wgEntity);
                            break;
                        case Entity.EntityType.PLAYER:
                            ent = NetPlayer.Deserialize(wgEntity);
                            break;
                    }

                    this.AddEntity(ent);
                    namedEntities.Add(ent.Name, ent);
                }
            }

            List<Entity> toUpdate = this.GetEntities();

            foreach(Entity ent in toUpdate)
            {
                if (ent.HasComponent<NetworkComponent>())
                {
                    if (clientID == ent.ClientID)
                    {
                        if (!updatedEntityNames.Contains(ent.Name))
                        {
                            this.RemoveEntity(ent);
                        }
                    }
                }
            }
        }

        public void SpawnNetPlayer(WGObject info)
        {
            NetPlayer np = NetPlayer.Deserialize(info);
            namedEntities.Add(np.Name, np);
            this.AddEntity(np);
        }

        public void RemoveNetEntities(WGObject info)
        {
            WGField wgClientID = info.FindField("ClientID");

            int pointer = 0;
            uint id = (uint)SerializerUtil.ReadLong(wgClientID.Data, ref pointer);

            string[] keys = namedEntities.Keys.ToArray();
            foreach(string key in keys)
            {
                Entity ent = namedEntities[key];
                if (ent.ClientID == id)
                {
                    namedEntities.Remove(key);
                    this.RemoveEntity(ent);
                }
            }
        }

        public void SerializeNetEntities(WGObject container)
        {
            string[] keys = namedEntities.Keys.ToArray();

            foreach(string key in keys)
            {
                WGObject wgo = namedEntities[key].Serialize();
                container.AddObject(wgo);
            }
        }

        public void SerializeEntities(WGObject container)
        {
            List<Entity> entities = this.GetEntities();

            foreach (Entity ent in entities)
            {
                if(ent is Player || ent is  Bullet || ent is Gun)
                {
                    if (!namedEntities.ContainsKey(ent.Name))
                    {
                        WGObject wgo = ent.Serialize();
                        container.AddObject(wgo);
                    }
                }
            }
        }

        public virtual void LoadEntities(WGObject wgEntities)
        {
            Console.WriteLine();
            Console.WriteLine("[DEBUG] \t Loading entities...");
            Console.WriteLine();
            WGObject localPlayer = wgEntities.FindObject("LocalPlayer");
            WGObject netEntities = wgEntities.FindObject("NetEntities");

            //Net entities...
            foreach(WGObject wgo in netEntities.Objects)
            {
                WGField entityTypes = wgo.FindField("EntityType");
                int pointer = 0;
                byte entityType = SerializerUtil.ReadByte(entityTypes.Data, ref pointer);

                switch((Entity.EntityType)entityType)
                {
                    case Entity.EntityType.BULLET:
                        Bullet bullet = Bullet.Deserialize(wgo);
                        this.AddEntity(bullet);
                        break;
                    case Entity.EntityType.PLAYER:
                        NetPlayer netplayer = NetPlayer.Deserialize(wgo);
                        this.AddEntity(netplayer);
                        break;
                    case Entity.EntityType.GUN:
                        Gun gun = Gun.Deserialize(wgo);
                        this.AddEntity(gun);
                        break;
                }
            }

            //local entities...
            Player player = Player.Deserialize(localPlayer);
            InputComponent icomp = player.GetComponent<InputComponent>();

            icomp.AppendHandler(EventSystem.GameEvent.Type.KEY_RELEASED, (args) =>
            {
                KeyReleasedGameEvent krEvent = (KeyReleasedGameEvent)args;

                switch ((Keys)krEvent.KeyCode)
                {
                    case Keys.R:
                        this.SpawnPlayer(player, (SpawnComponent.TeamColor)Game.Dice.Next(2));
                        break;
                }

                return false;
            });

            this.AddEntity(player);

            Gun playerGun = new Gun(player.GetComponent<LocationComponent>(), player.GetComponent<ClassComponent>().PlayerClass);

            GunComponent gunComp = playerGun.GetComponent<GunComponent>();
            InputComponent inputComp = new InputComponent();
            inputComp.AddEventHandler(EventSystem.GameEvent.Type.MOUSE_MOVED, (args) =>
            {
                MouseMovedGameEvent mmEvent = (MouseMovedGameEvent)args;
                gunComp.MousePosition = new Vector2d(mmEvent.X, mmEvent.Y);
                return false;
            });

            inputComp.AddEventHandler(GameEvent.Type.MOUSE_PRESSED, (args) =>
            {
                MousePressedGameEvent mpEvent = (MousePressedGameEvent)args;
                if (mpEvent.Button == (int)System.Windows.Forms.MouseButtons.Left)
                {
                    gunComp.IsShooting = true;
                    playerGun.GetComponent<GraphicsComponent>().AnimHandler.Change("shooting");
                    gunComp.RenderOffset = gunComp.FiringOffset;
                }
                return false;
            });

            inputComp.AddEventHandler(GameEvent.Type.MOUSE_RELEASED, (args) =>
            {
                MouseReleasedGameEvent mrEvent = (MouseReleasedGameEvent)args;
                if (mrEvent.Button == (int)System.Windows.Forms.MouseButtons.Left)
                {
                    gunComp.IsShooting = false;
                    playerGun.GetComponent<GraphicsComponent>().AnimHandler.Change("gun");
                    gunComp.RenderOffset = gunComp.IdleOffset;
                }

                return false;
            });

            playerGun.AddComponent(inputComp);

            this.AddEntity(playerGun);

            /*
            //GUNS GUNS GUNS
            Gun minigun = new Gun( /* RIP managun T~T  */ /*  EXTRA RIP  
                player.GetComponent<LocationComponent>(),
                PlayerClass.OVERWEIGHT
                );

            this.AddEntity(minigun); 
            */
            this.SpawnPlayer(player, SpawnComponent.TeamColor.BLUE);
        }
    }
}
