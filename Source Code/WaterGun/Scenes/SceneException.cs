﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Scenes
{
    public class SceneException : Exception
    {
        public SceneException(string message) : base("[Scene] \t " + message)
        {
            //do nothing (yet!)
        }
    }
}
