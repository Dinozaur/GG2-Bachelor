﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.Entities;
using WaterGun.Entities.Mobs;
using WaterGun.Entities.Components;
using WaterGun.EventSystem;

namespace WaterGun.Scenes
{
    public abstract class Scene : GameEventLayer
    {
        private List<Entity> toUpdate;
        private List<Entity> toAdd;
        private List<Entity> toRemove;

        public Scene(string name)
        {
            toUpdate = new List<Entity>();
            toAdd = new List<Entity>();
            toRemove = new List<Entity>();

            Name = name;
        }

        public string Name { get; private set; }

        /// <summary>
        /// Returns all entities which should be updated <para />
        /// TODO: seperate static from dynamic entities?
        /// </summary>
        /// <returns></returns>
        public List<Entity> GetEntities()
        {
            return toUpdate.GetRange(0, toUpdate.Count);
        }

        /// <summary>
        /// Adds an entity to the toRemove list. The entity will be actively removed in PostUpdate().
        /// </summary>
        /// <param name="ent"></param>
        public void RemoveEntity(Entity ent)
        {
            toRemove.Add(ent);
        }

        /// <summary>
        /// Adds an entity to the toAdd list. The entity will be actively added in PostUpdate().
        /// </summary>
        /// <param name="ent"></param>
        public void AddEntity(Entity ent)
        {
            toAdd.Add(ent);
        }

        /// <summary>
        /// Handles adding and removing of entities on the toUpdate entity list.
        /// </summary>
        public void PostUpdate()
        {
            toUpdate.AddRange(toAdd);
            toUpdate.RemoveAll((e) => { return toRemove.Contains(e); });

            toAdd.Clear();
            toRemove.Clear();
        }

        /// <summary>
        /// Adds tiles and static entities to the scene.                                    <para />
        /// This is abstracted so each inheritance from here can initialize its own needs.
        /// </summary>
        public abstract void Init(Camera camera);

        public abstract Image GetScreenshot(Camera camera);

        public override void OnEvent(GameEvent gEvent)
        {
            List<Entity> entities = new List<Entity>();

            entities.AddRange(toUpdate);

            foreach(Entity ent in entities)
            {
                if (ent != null)
                {
                    if (ent.HasComponent<InputComponent>())
                    {
                        ent.GetComponent<InputComponent>().OnEvent(gEvent);
                    }
                }
            }
        }
    }
}
