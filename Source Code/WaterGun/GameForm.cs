﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WaterGun.Debug;
using WaterGun.EventSystem;
using WaterGun.EventSystem.Types;

namespace WaterGun
{
    public partial class GameForm : Form, IGameWindow
    {
        private readonly bool USE_CANVAS = true;

        private List<GameEventLayer> layerStack;    //<-- see AddEventLayer() comment.
        private bool mouseDown; //<-- used for determining if v2x mouse-move is also v2x drag.
        private Control canvas;

        public GameForm(int width, int height, int scale)
        {
            InitializeComponent();

            if (USE_CANVAS)
            {
                PictureBox canvas = new PictureBox();
                canvas.Location = new Point(0, 0);
                canvas.Size = new Size(width * scale, height * scale);
                canvas.Image = new Bitmap(canvas.Width, canvas.Height);
                this.canvas = canvas;
                this.Controls.Add(canvas);
            }
            else
            {
                this.canvas = this;
            }

            this.DoubleBuffered = true;
            this.Width = width * scale;
            this.Height = height * scale;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            mouseDown = false;

            layerStack = new List<GameEventLayer>();

            //Event Handling
            canvas.MouseDown += (sender, args) =>
            {
                mouseDown = true;

                GameEvent gEvent = new MousePressedGameEvent((int)args.Button, args.X / scale, args.Y / scale);
                OnGameEvent(gEvent);
            };

            canvas.MouseUp += (sender, args) => {
                mouseDown = false;
                OnGameEvent(new MouseReleasedGameEvent((int)args.Button, args.X / scale, args.Y / scale));
                
            };

            canvas.MouseMove += (sender, args) => { OnGameEvent(new MouseMovedGameEvent(args.X / scale, args.Y / scale, mouseDown)); };

            this.KeyDown += (sender, args) => { OnGameEvent(new KeyPressedGameEvent((int)args.KeyCode)); };
            this.KeyUp += (sender, args) => { OnGameEvent(new KeyReleasedGameEvent((int)args.KeyCode)); };
        }

        /// <summary>
        /// Adds v2x GameEventLayer to this window. The layers act like v2x stack, 
        /// meaning the last to be added is the first to recieve the event when it occures.
        /// </summary>
        /// <param name="layer">The layer to add</param>
        public void AddEventLayer(GameEventLayer layer)
        {
            layerStack.Add(layer);
        }

        public void RemoveEventLayer(GameEventLayer layer)
        {
            layerStack.Remove(layer);
        }

        public void Render(Image screenshot)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    this.Invoke((Action<Image>)Render, screenshot);
                }
                else
                {
                    int[] pxlArr = new int[screenshot.Width * screenshot.Height];

                    BitmapData screenshotData = ((Bitmap)screenshot).LockBits(new Rectangle(0, 0, screenshot.Width, screenshot.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
                    Marshal.Copy(screenshotData.Scan0, pxlArr, 0, pxlArr.Length);
                    ((Bitmap)screenshot).UnlockBits(screenshotData);

                    int sWidth = screenshot.Width;
                    int sHeight = screenshot.Height;

                    /* THE PARRALELISM! IT DOES NOTHING!!
                    Parallel.For(0, 4, (i) =>
                    {
                        //TODO: double buffer this properly
                        using (Graphics g = this.CreateGraphics())
                        {
                            //g.Clear(Color.Brown); //<-- might not be necessary
                            //g.DrawImage(screenshot, 0, 0, this.Width, this.Height);
                            Bitmap bmp = new Bitmap(sWidth, sHeight / 4);
                            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
                            Marshal.Copy(pxlArr, i * bmp.Height * bmp.Width, bmpData.Scan0, bmp.Height * bmp.Width);
                            bmp.UnlockBits(bmpData);

                            g.DrawImage(bmp, 0, i * bmp.Height);
                            bmp.Dispose();
                        }
                    });
                    */


                    if (USE_CANVAS)
                    { 
                        using (Graphics g = Graphics.FromImage(((PictureBox)canvas).Image))
                        {
                            g.Clear(Color.Magenta);
                            g.DrawImage(screenshot, 0, 0);
                            //canvas.Image = screenshot;
                        }

                        canvas.Refresh();
                    }
                    else
                    {
                        using (Graphics g = this.CreateGraphics())
                        {
                            g.DrawImage(screenshot, 0, 0);
                        }
                    }
                   
                    screenshot.Dispose();
                }
            }
            catch (Exception e)
            {
                DLog.Log(e.StackTrace);
                DLog.Log(e.Message);
                DLog.Log("Error while rendering");
            }
        }

        public void SetTitle(string title)
        {
            try
            {
                //Invoke check required because the methods are called from v2x other thread.
                if (this.InvokeRequired)
                {
                    this.Invoke((Action<string>)SetTitle, title);
                }
                else
                {
                    this.Text = title;
                }
            }
            catch (Exception e)
            {
                //TODO: use debug tool.
                Console.WriteLine(e.StackTrace);
                Console.WriteLine("Could not set title!!");
            }
        }

        /// <summary>
        /// This is called by the events set in the constructor.
        /// </summary>
        /// <param name="gEvent"></param>
        private void OnGameEvent(GameEvent gEvent)
        {
            for(int i = layerStack.Count - 1; i >= 0; i--)
            {
                layerStack[i].OnEvent(gEvent);
            }
        }
    }
}
