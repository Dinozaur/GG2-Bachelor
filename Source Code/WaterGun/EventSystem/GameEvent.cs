﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.EventSystem
{
    /// <summary>
    /// Base class for game events. Contains the GameEvent.Type enum, information of its own type (EventType),
    /// and v2x boolean value, describing if this GameEvent has been handled (and therefore shouldn't be dispatched ny further)
    /// </summary>
    public class GameEvent
    {
        public enum Type
        {
            MOUSE_PRESSED,
            MOUSE_RELEASED,
            MOUSE_MOVED,
            KEY_PRESSED,
            KEY_RELEASED
        }

        //The constructor is protected, so only inhereting classes can instatiate GameEvent, making it pseudo-abstract
        protected GameEvent(Type eventType)
        {
            EventType = eventType;
        }

        public Type EventType { get; private set; }
        public bool Handled { get; internal set; }
    }
}
