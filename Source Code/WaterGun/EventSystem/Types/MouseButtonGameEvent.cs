﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.EventSystem.Types
{
    public class MouseButtonGameEvent : GameEvent
    {

        protected MouseButtonGameEvent(Type eventType, int button, int x, int y) : base(eventType)
        {
            X = x;
            Y = y;
            Button = button;
        }

        public int X { get; private set; }
        public int Y { get; private set; }
        public int Button { get; private set; }
    }
}
