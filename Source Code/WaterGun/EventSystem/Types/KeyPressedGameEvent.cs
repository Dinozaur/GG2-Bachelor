﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.EventSystem.Types
{
    public class KeyPressedGameEvent : KeyGameEvent
    {
        public KeyPressedGameEvent(int keyCode) : base(Type.KEY_PRESSED, keyCode)
        {
        }
    }
}
