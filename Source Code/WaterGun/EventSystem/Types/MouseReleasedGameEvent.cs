﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.EventSystem.Types
{
    public class MouseReleasedGameEvent : MouseButtonGameEvent
    {
        public MouseReleasedGameEvent(int button, int x, int y) : base(Type.MOUSE_RELEASED, button, x, y)
        {
        }
    }
}
