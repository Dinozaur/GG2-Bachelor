﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.EventSystem.Types
{
    public class MouseMovedGameEvent : GameEvent
    {
        public MouseMovedGameEvent(int x, int y, bool dragged) : base(Type.MOUSE_MOVED)
        {
            X = x;
            Y = y;
            Dragged = dragged;
        }

        public int X { get; private set; }
        public int Y { get; private set; }
        public bool Dragged { get; private set; }
    }
}
