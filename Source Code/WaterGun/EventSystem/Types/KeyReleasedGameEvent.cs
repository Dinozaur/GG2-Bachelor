﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.EventSystem.Types
{
    public class KeyReleasedGameEvent : KeyGameEvent
    {
        public KeyReleasedGameEvent(int keyCode) : base(Type.KEY_RELEASED, keyCode)
        {

        }
    }
}
