﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.EventSystem.Types
{
    public class MousePressedGameEvent : MouseButtonGameEvent
    {
        public MousePressedGameEvent(int button, int x, int y) : base(Type.MOUSE_PRESSED, button, x, y)
        {
        }
    }
}
