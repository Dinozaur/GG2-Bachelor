﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.EventSystem.Types
{
    public class KeyGameEvent : GameEvent
    {
        protected KeyGameEvent(Type eventType, int keyCode) : base(eventType)
        {
            KeyCode = keyCode;
        }

        public int KeyCode { get; private set; }
    }
}
