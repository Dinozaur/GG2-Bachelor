﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.EventSystem
{
    public delegate bool EventHandler(GameEvent e);

    public class GameEventDispatcher
    {
        public GameEventDispatcher(GameEvent gameEvent)
        {
            GameEvent = gameEvent;
        }

        public GameEvent GameEvent { get; private set; }

        public void Dispatch(GameEvent.Type type, EventHandler handler)
        {
            if(!GameEvent.Handled)
            {
                if(GameEvent.EventType == type)
                {
                    GameEvent.Handled = handler(GameEvent);
                }
            }
        }


    }
}
