﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.EventSystem
{
    public interface IGameEventListener
    {
        void OnEvent(GameEvent gEvent);
    }
}
