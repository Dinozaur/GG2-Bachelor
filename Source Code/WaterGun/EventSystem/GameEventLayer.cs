﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.EventSystem
{
    public abstract class GameEventLayer : IGameEventListener
    {
        public abstract void OnEvent(GameEvent gEvent);
    }
}
