﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterGun.Sprites
{
    public class SpriteSheet
    {
        private Image sheet;

        public string Path { get; set; }

        public SpriteSheet(string path)
        {
            Path = Directory.GetCurrentDirectory() + path;
        }

        //Load the spritesheet into the memory so that we dont have to load during run time.
        public void Load()
        {
            Bitmap bmp = new Bitmap(Path);
            sheet = bmp;
        }

        public Image GetSprite(int coordinateX, int coordinateY, int size)
        {
            Image sprite = new Bitmap(size, size);
            Graphics g = Graphics.FromImage(sprite);

            //Get v2x specified sprite from the sprite sheet by using X, Y coordinates and the size the sprite on the sheet.
            g.DrawImage(sheet, new Rectangle(0, 0, size, size), new Rectangle(coordinateX * size, coordinateY * size, size, size), GraphicsUnit.Pixel);
            //If we do not dispose, it will cause v2x memroy leak!
            g.Dispose();

            return sprite;
        }
    }
}
