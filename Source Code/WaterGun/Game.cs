﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using WaterGun.Scenes;
using WaterGun.Scenes.Levels;
using WaterGun.Debug;
using WaterGun.Sounds;
using WGHosting;
using WaterGun.Net;
using GameSerializer;
using System.Net.Sockets;
using WaterGun.Entities.Mobs;
using System.IO;
using WaterGun.Maths;

namespace WaterGun
{
    class Game
    {
        private static readonly int FPS = 30;
        private static readonly int WIDTH = 800;
        // Cast to double BEFORE calculations are done.
        private static readonly double FORMAT = (double)9 / 16;
        // Cast to double to int AFTER calculations are done.
        private static readonly int HEIGHT = 600; //(int)(WIDTH * FORMAT);
        private static readonly int SCALE = 1;

        private const string NEW_PLAYER = "Connect";
        private const string PLAYER_JOINED = "PlayerJoined";
        private const string PLAYER_DISCONNECT = "PlayerLeft";
        private const string GAME_UPDATE = "Update";

        private static IGameWindow gameWindow;
        private static World world;
        private static Camera camera; //<-- refactor to world?
        private static GameTimer gameTimer;
        private static WGClient netClient;
        
        //hosting stuf
        private static Dictionary<string, uint> ipToClientId;
        private static WGHost host;
        private static bool isHost;

        //for debugging
        //private static Dictionary<WGDatabase, byte[]> sentDBData;
        private static uint dbCounter;
        private static uint netUpdatesPerSecond;
        private static List<int> fpses;
        private static uint fpsCaptureTime;

        //properties
        public static Random Dice { get; private set; } 
        public static bool Running { get; private set; }


        private static uint nextEntityId;
        private static int largestDBCount;

        /// <summary>
        /// FOR DEBUGGING PURPOSES ONLY!
        /// </summary>
        public static Camera Camera{
            get
            {
                return camera;
            }
        }

        public static uint ClientID { get; private set; }


        //Main Entry Point
        static void Main(string[] args)
        {
            //DEBUG START
            //sentDBData = new Dictionary<WGDatabase, byte[]>();
            largestDBCount = 0;
            dbCounter = 0;
            fpsCaptureTime = 0;
            fpses = new List<int>();
            CaptureFPS = false;
            
            //DEBUG END

            AskToHost();
            //CONNECT TO HOST LOGIC

            bool online = ConnectToHost();

            //END CONNECT TO HOST LOGIC

            if (online)
            {
                Dice = new Random();
                //TODO: Use Debug Tools.
                Console.WriteLine("Loading Game Window");
                LoadGameWindow();

                nextEntityId = 0;
                gameTimer = new GameTimer();
                Scene kothHarvest = new KothHarvest();


                camera = new Camera(0, 0, WIDTH, HEIGHT, 26 * 16 - WIDTH * SCALE, 14 * 16 - HEIGHT * SCALE);

                kothHarvest.Init(camera);
                world = new World(kothHarvest, gameWindow);

                Console.WriteLine("Loading complete, starting game loop.");
                //PlaySoundTest();
                StartGameLoop();
            }
        }

        private static void AskToHost()
        {
            bool ask = true;
            isHost = false;
            while (ask)
            {
                Console.Write("Host? (y/n): ");
                string answer = Console.ReadLine();

                answer = answer.ToLower();
                ask = !(answer.Equals("y") || answer.Equals("n") || answer.Equals(""));

                if (ask)
                {
                    Console.WriteLine("\"{0}\" was not recognized as a valid command!", answer);
                }
                else
                {
                    isHost = answer.Equals("y");
                }
                Console.WriteLine();
            }

            if (isHost)
            {
                ipToClientId = new Dictionary<string, uint>();

                Console.Write("Port: ");
                string port = Console.ReadLine();
                host = new WGHost(Int32.Parse(port));

                host.OnConnect += (ip) =>
                {
                    uint clientId = nextEntityId++;
                    Player player = new Player(clientId, new Vector2d(0, 0), Entities.PlayerClass.OVERWEIGHT);
                    ipToClientId.Add(ip, clientId);

                    WGDatabase db = new WGDatabase(NEW_PLAYER);

                    WGObject joinInfo = new WGObject("Join Info");
                    joinInfo.AddField(new WGField("ClientID", clientId));

                    WGObject localPlayer = player.Serialize();
                    localPlayer.ChangeName("LocalPlayer");

                    joinInfo.AddObject(localPlayer);

                    WGObject netEntities = new WGObject("NetEntities");

                    if (world != null)
                    {
                        world.SerializeEntities(netEntities);
                        world.SerializeNetEntities(netEntities);
                    }

                    joinInfo.AddObject(netEntities);
                    db.Add(joinInfo);

                    if(db.Size > largestDBCount)
                    {
                        largestDBCount = db.Size;
                        Console.WriteLine("[DEBUG] largest DB count: " + largestDBCount);

                    }

                    byte[] data = db.SerializeToBytes();
                    //sentDBData.Add(db, data);

                    host.SendToSingle(data, ip);
                    db = new WGDatabase(PLAYER_JOINED);

                    localPlayer.ChangeName("Player");
                    db.Add(localPlayer);

                    if (db.Size > largestDBCount)
                    {
                        largestDBCount = db.Size;
                        Console.WriteLine("[DEBUG] largest DB count: " + largestDBCount);

                    }
                    byte[] data2 = db.SerializeToBytes();
                    //sentDBData.Add(db, data2);
                    host.Send(data2, ip);
                };

                host.OnDisconnect += (ip) =>
                {
                    WGDatabase connectInfo = new WGDatabase(PLAYER_DISCONNECT);
                    WGObject info = new WGObject("Info");
                    info.AddField(new WGField("ClientID",  ipToClientId[ip]));
                    connectInfo.Add(info);


                    if (connectInfo.Size > largestDBCount)
                    {
                        largestDBCount = connectInfo.Size;
                        Console.WriteLine("[DEBUG] largest DB count: " + largestDBCount);

                    }

                    byte[] oldPlayerData = connectInfo.SerializeToBytes();

                    //sentDBData.Add(connectInfo, oldPlayerData);
                    host.Send(oldPlayerData, "0");
                };

                host.Start();
            }


        }

        private static bool ConnectToHost()
        {
            string ip = "";
            int port = -1;

            if (isHost)
            {
                ip = host.IP;
                port = host.Port;
                Console.WriteLine("[Press ENTER] to start game...");
                Console.ReadLine();
            }
            else
            {
                Console.Write("Host IP (10.28.51.86): ");
                ip = Console.ReadLine();
                ip = ip.Equals("") ? "10.28.51.86" : ip;
                Console.Write("Host Port (9966): ");
                string strPort = Console.ReadLine();
                port = strPort.Length == 0 ? 9966 : Int32.Parse(strPort);
            }

            netClient = new WGClient(ip, port);
            netClient.OnDataRecieved += OnNetDataRecieved;
            try
            {
                netClient.Connect();
                Console.WriteLine("Connected!");
                return true;
            }
            catch (SocketException e)
            {
                Console.WriteLine("Failed to connect to isHost " + ip);
                Console.WriteLine("Exception: " + e);
                Console.ReadLine();
                return false;
            }
        }

        private static void OnNetDataRecieved(byte[] data)
        {
            byte[] cleanedData = SerializerUtil.CleanBytes(data);
            if (cleanedData != null)
            {
                while (world == null) ; //<-- TODO: Make diz perdy
                try
                {
                    WGDatabase db = WGDatabase.Deserialize(cleanedData);

                    string dbName = new string(db.Name);
                    dbName = dbName.Split('#')[0];
                    switch (dbName)
                    {
                        case GAME_UPDATE:
                            WGObject ne = db.FindObject("NetEntities");
                            world.UpdateNetEntities(ne);
                            netUpdatesPerSecond++;
                            break;
                        case PLAYER_JOINED:
                            WGObject joinedInfo = db.FindObject("Player");
                            world.SpawnNetPlayer(joinedInfo);
                            break;
                        case PLAYER_DISCONNECT:
                            WGObject discInfo = db.FindObject("Info");
                            world.RemoveNetEntities(discInfo);
                            break;
                        case NEW_PLAYER:
                            WGObject joinInfo = db.FindObject("Join Info");
                            WGField fid = joinInfo.FindField("ClientID");
                            int pointer = 0;
                            ClientID = (uint)SerializerUtil.ReadLong(fid.Data, ref pointer);
                            world.LoadEntities(joinInfo);
                            break;
                    }
                }
                catch (SocketException e)
                {
                    Console.WriteLine("[GAME] \t Error while recieving connectionData from client");
                    Console.WriteLine(e);
                }
                catch (GameSerializerException e)
                {
                    Console.WriteLine("[GAME] \t Error while serilaizing!");
                    Console.WriteLine(e);
                }
            }       
        }

        /// <summary>
        /// For testing purposes
        /// </summary>
        public static bool CaptureFPS { get; set; }

        /// <summary>
        /// For use to create unique Entity.Name
        /// </summary>
        /// <returns>The next </returns>
        public static uint GetNextEntityId()
        {
            return nextEntityId++;
        }

        private static void LoadGameWindow()
        {
            //Define the size of the game window
            GameForm form = new GameForm(WIDTH, HEIGHT, SCALE);
            //When the form is disposed, set Running to false and stop game loop.
            form.Disposed += (sender, args) => { 
                Running = false;
                netClient.Disconnect();
                if (isHost)
                {
                    host.Stop();
                }
            };
            //Set gamewindow to the one we just created, otherwise we get v2x null pointer exeception
            gameWindow = (IGameWindow)form;

            //Start v2x thread that will run the game window.
            new Thread(() =>
            {
                Application.Run(form);
            }).Start();
        }

        private static void StartGameLoop()
        {
            //The game is now running
            Running = true;

            //Timer is being used to calculate FPS (frames per secound) and UPS (updates per secound)
            Stopwatch timer = Stopwatch.StartNew();

            int frames = 0;
            int updates = 0;

            long lastUpdate = 0;
            int secondTracker = 0;

            int mspf = 1000 / FPS;
            int delta = 0;

            int sleeptime = 0;
            long lastLoop = 0;
            //While the game is running, do the math needed to calcaute FPS and UPS
            while (Running)
            {
                delta = (int)(timer.ElapsedMilliseconds - lastUpdate);

                gameTimer.Set(delta, delta / mspf);

                secondTracker += (int)(timer.ElapsedMilliseconds - lastLoop);
                lastLoop = timer.ElapsedMilliseconds;

                if (secondTracker > 1000)
                {
                    AddFps(frames);
                    gameWindow.SetTitle("FPS: " + frames + " UPS: " + updates + " NUPS: " + netUpdatesPerSecond + " RECTIME: " + fpsCaptureTime);
                    updates = 0;
                    frames = 0;
                    secondTracker = 0;
                    netUpdatesPerSecond = 0;
                }     
           
                if (delta >= mspf)
                {
                    lastUpdate = timer.ElapsedMilliseconds;
                    Update();
                    Render();
                    frames++;
                    updates++;
                }

                sleeptime = mspf - (int)(timer.ElapsedMilliseconds - lastUpdate);
                Thread.Sleep(sleeptime > 0 ? sleeptime : 1);
            }
        }

        private static void Update()
        {
            world.Update(camera, gameTimer);

            //NETWORK STUFF
            WGDatabase db = new WGDatabase(GAME_UPDATE + "#" + dbCounter++);
            WGObject wgo = new WGObject("NetEntities");
            wgo.AddField(new WGField("ClientID", ClientID));
            world.SerializeEntities(wgo);
            db.Add(wgo);

            try
            {
                if (db.Size > largestDBCount)
                {
                    largestDBCount = db.Size;
                    Console.WriteLine("[DEBUG] largest DB count: " + largestDBCount);

                }

                byte[] data = db.SerializeToBytes();
                //sentDBData.Add(db, data);

                netClient.Send(data);
            }
            catch (SocketException e)
            {
                Console.WriteLine("[GAME] \t Socket exception");
                Console.WriteLine(e);
            }
            catch (IOException e)
            {
                Console.WriteLine("[GAME] \t IO exception");
                Console.WriteLine(e);
            }
            
        }

        private static void Render()
        {
            Image sceneScreenshot = world.GetScene().GetScreenshot(camera);
            //TODO: add HUD image

            gameWindow.Render(sceneScreenshot);
        }

        private static void AddFps(int fps)
        {
            if (CaptureFPS)
            {
                fpses.Add(fps);
                fpsCaptureTime++;
                if (fpsCaptureTime >= 5 * 60) //5 min
                {
                    PrintFpsFile();
                    CaptureFPS = false;
                    fpsCaptureTime = 0;
                    fpses.Clear();
                }
            }
        }

        private static void PrintFpsFile()
        {
            string fileString = "FPS:\r\n";

            int min = fpses[0];
            int max = 0;
            int sum = 0;

            foreach(int i in fpses)
            {
                sum += i;
                min = i < min ? i : min;
                max = i > max ? i : max;
                fileString += i + "\r\n";
            }

            fileString += "Min:;Avg:;Max:;\r\n";
            fileString += min + ";" + ((double)sum / fpses.Count) + ";" + max + "\r\n";

            string path = Directory.GetCurrentDirectory();
            path += "/fps-";
            path += DateTime.Now.Hour + "-";
            path += DateTime.Now.Minute;
            path += ".csv";
            File.WriteAllText(path, fileString);
        }

        //Manuel Test Method
        private static void PlaySoundTest()
        {
            SoundManager manager = SoundManager.GetInstance();

            Sound test = new Sound("Super Awesome Test", "Sounds/Music/Aeolus.wav", false);
            manager.AddSound(test);
            manager.PlaySound("Super Awesome Test", SoundManager.SoundLevel.Menu);

            Console.ReadLine();
            DLog.Log("Press Enter to pause");

            manager.PauseSound("Super Awesome Test", SoundManager.SoundLevel.Menu);

            Console.ReadLine();
            DLog.Log("Press Enter to Resume");

            manager.ResumeSound("Super Awesome Test", SoundManager.SoundLevel.Menu);

            Console.ReadLine();
            DLog.Log("Press Enter to Stop");

            manager.StopSound("Super Awesome Test", SoundManager.SoundLevel.Menu);
        }
    }
}
