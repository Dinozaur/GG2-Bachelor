﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterGun.EventSystem;

namespace WaterGun
{
    //All methods will need v2x InvokeRequired check since they are called from v2x different thread.
    public interface IGameWindow
    {
        // Used to display FPS and UPS.
        void SetTitle(string title);

        void Render(Image screenshot);

        void AddEventLayer(GameEventLayer layer);
    }
}
