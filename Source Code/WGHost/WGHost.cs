﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WGHosting
{
    public delegate void WGHostingEvent(string ip);

    public class WGHost
    {
        private static int RECIEVE_BUFFER_SIZE = 8000;

        private TcpListener mainListener;
        private Dictionary<TcpClient, string> senders;   //<-- the host recieves from senders
        private Dictionary<TcpClient, string> recievers; //<-- the host send to recievers

        private List<Thread> threads;

        public WGHost(int port)
        {
            Port = port;
            Open = false;
            senders = new Dictionary<TcpClient, string>();
            recievers = new Dictionary<TcpClient, string>();
            threads = new List<Thread>();

            OnConnect += (ip) => { };
            OnDisconnect += (ip) => { };

            IPAddress[] ips = Dns.GetHostEntry(Dns.GetHostName()).AddressList;

            bool found = false;
            int index = 0;

            while (!found && index < ips.Length)
            {
                IPAddress addr = ips[index];

                if (addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    found = true;
                }
                else
                {
                    index++;
                }
            }

            if (found)
            {
                IPAddress ip = Dns.GetHostEntry(Dns.GetHostName()).AddressList[index];
                IP = ip.ToString();

                mainListener = new TcpListener(ip, port);
            }
            else
            {
                Console.WriteLine("Could not find an IPv4 to connect to...");
            }
        }

        public string IP { get; private set; }
        public int Port { get; private set; }
        public bool Open { get; private set; }

        public void Start()
        {
            Open = true;

            Thread t = new Thread(() => {
                try
                {
                    Console.WriteLine("Starting to listen on " + mainListener.LocalEndpoint);
                    mainListener.Start(100);
                    while (Open)
                    {
                        //TODO: use Async?
                        TcpClient handler = mainListener.AcceptTcpClient();

                        if (handler != null)
                        {
                            Console.WriteLine("Incomming connection: " + handler.Client.RemoteEndPoint);
                            AddHandler(handler);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("ERROR!");
                    Console.WriteLine(e);
                    Console.WriteLine(e.StackTrace);  
                }

            });

            threads.Add(t);
            t.Start();
        }

        public void Stop()
        {
            Open = false;
            mainListener.Stop();

            foreach (Thread t in threads)
            {
                t.Abort();
            }

            DisconnectAllSockets(senders.Keys.ToArray());
            DisconnectAllSockets(recievers.Keys.ToArray());
        }

        private void DisconnectAllSockets(TcpClient[] sockets)
        {
            foreach (TcpClient s in senders.Keys)
            {
                try
                {
                    if (s.Connected)
                    {
                        s.Close();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Sockets are bitching again.. ");
                }
            }
        }

        /// <summary>
        /// Sends data[] ONLY to the given IP.
        /// </summary>
        /// <param name="data">The data to send</param>
        /// <param name="ip">The ip to send to</param>
        public void SendToSingle(byte[] data, string ip)
        {
            TcpClient reciever = FindConnection(ip, recievers);
            NetworkStream netStream = reciever.GetStream();

            byte[] buffer = new byte[RECIEVE_BUFFER_SIZE];
            Array.Copy(data, buffer, data.Length);

            netStream.Write(buffer, 0, buffer.Length);
        }

        /// <summary>
        /// Sends data[] to all connections EXCEPT for the one with the given IP.
        /// </summary>
        /// <param name="data">The data to send</param>
        /// <param name="ip">The ip of the connection from which the data originated</param>
        public void Send(byte[] data, string ip)
        {
            TcpClient[] recieverArr = recievers.Keys.ToArray();
            foreach (TcpClient reciever in recieverArr)
            {
                if (!recievers[reciever].Equals(ip))
                {
                    if (reciever.Connected)
                    {
                        NetworkStream netStream = reciever.GetStream();

                        byte[] buffer = new byte[RECIEVE_BUFFER_SIZE];
                        Array.Copy(data, buffer, data.Length);

                        netStream.Write(buffer, 0, buffer.Length);
                       
                    }
                    else
                    {
                        bool found = false;
                        int index = 0;

                        TcpClient[] senderArr = senders.Keys.ToArray();
                        while (!found && index < senderArr.Length)
                        {
                            TcpClient sender = senderArr[index];
                            if (senders[sender].Equals(recievers[reciever]))
                            {
                                sender.Close();
                                senders.Remove(sender);
                                found = true;
                            }
                            index++;
                        }

                        reciever.Close();
                        recievers.Remove(reciever);
                        OnDisconnect(recievers[reciever]);
                    }
                }
            }
        }

        private TcpClient FindConnection(string ip, Dictionary<TcpClient, string> connections)
        {
            TcpClient[] list = connections.Keys.ToArray();

            bool found = false;
            int index = 0;

            while (!found && index < list.Length)
            {
                TcpClient handler = list[index];

                if (connections[handler].Equals(ip))
                {
                    found = true;
                }
                else
                {
                    index++;
                }
            }

            return list[index];
        }

        private void AddHandler(TcpClient handler)
        {
            bool senderFound = false;
            int senderIndex = 0;
            TcpClient[] senderArr = senders.Keys.ToArray();
            while (!senderFound && senderIndex < senderArr.Length)
            {
                TcpClient sender = senderArr[senderIndex];

                string senderIP = sender.Client.RemoteEndPoint.ToString().Split(':')[0];
                string handlerIP = handler.Client.RemoteEndPoint.ToString().Split(':')[0];


                if (senderIP.Equals(handlerIP) && sender.Connected)
                {
                    senderFound = true;
                }
                else
                {
                    senderIndex++;
                }
            }

            if (senderFound)
            {
                bool recieverFound = false;
                int recieverIndex = 0;
                TcpClient[] recieverArr = recievers.Keys.ToArray();

                while (!recieverFound && recieverIndex < recieverArr.Length)
                {
                    TcpClient reciever = recieverArr[recieverIndex];

                    string recieverIP = reciever.Client.RemoteEndPoint.ToString().Split(':')[0];
                    string handlerIP = handler.Client.RemoteEndPoint.ToString().Split(':')[0];

                    if (recieverIP.Equals(handlerIP) && reciever.Connected && reciever.Client.Poll(500 * 1000, SelectMode.SelectWrite))
                    {
                        recieverFound = true;
                    }
                    else
                    {
                        recieverIndex++;
                    }
                }

                if (!recieverFound)
                {
                    TcpClient sender = senderArr[senderIndex];
                    recievers.Add(handler, senders[sender]);

                    sender.ReceiveBufferSize = RECIEVE_BUFFER_SIZE;
                    sender.SendBufferSize = RECIEVE_BUFFER_SIZE;
                    handler.SendBufferSize = RECIEVE_BUFFER_SIZE;
                    handler.ReceiveBufferSize = RECIEVE_BUFFER_SIZE;

                    OnConnect(senders[sender]);
                }
                else
                {
                    Console.WriteLine("IP {0} is trying to connect more than twice!", handler.Client.RemoteEndPoint.ToString());
                    Console.WriteLine("Shutting {0} down 3rd connection...", handler.Client.RemoteEndPoint.ToString());
                    handler.Close();
                }
            }
            else
            {
                senders.Add(handler, handler.Client.RemoteEndPoint.ToString().Split(':')[0]);

                ListenToSender(handler);
            }
        }

        private void ListenToSender(TcpClient handler)
        {
            Thread t  = new Thread(() => {
                byte[] buffer = new byte[RECIEVE_BUFFER_SIZE];
                int readPointer = 0;
                while(handler.Connected) {
                    if (handler.Available > 0)
                    {
                        NetworkStream netStream = handler.GetStream();
                        
                        int readCount = netStream.Read(buffer, readPointer, buffer.Length - readPointer);
                        readPointer += readCount;

                        if (readPointer == RECIEVE_BUFFER_SIZE)
                        {
                            Send(buffer, senders[handler]);
                            Array.Clear(buffer, 0, buffer.Length);
                            readPointer = 0;
                        }
         
                    }
                }
                Console.WriteLine("Connection {0} lost (handler.Connected == false)", handler.Client.RemoteEndPoint);
            });

            threads.Add(t);

            t.Start();
        }

        public event WGHostingEvent OnConnect;
        public event WGHostingEvent OnDisconnect;
    }
}
