﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace WGHosting
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Port: ");
            string port = Console.ReadLine();
            WGHost host = new WGHost(Int32.Parse(port));
            Console.WriteLine("[PRESS ENTER] to open connection...");
            Console.ReadLine();
            try
            {
                host.Start();
                Console.WriteLine("Success!");
                Console.WriteLine("[Press ENTER] to close connection...");
                Console.ReadLine();
                host.Stop();
            }
            catch (SocketException e)
            {
                Console.WriteLine("Socket Error:");
                Console.WriteLine(e);
                Console.WriteLine("[Press ENTER] to continue...");
                Console.ReadLine();
            }
            catch (IOException e)
            {
                Console.WriteLine("Stream Error:");
                Console.WriteLine(e);
                Console.WriteLine("[Press ENTER] to continue...");
                Console.ReadLine();
            }
            
            Console.WriteLine("Host closed..");
            Console.WriteLine("[Press ENTER] to quit..");
            Console.ReadLine();
        }
    }
}
